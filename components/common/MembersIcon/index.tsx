import {MaterialCommunityIcons} from '@expo/vector-icons';
import * as React from 'react';
import {secondaryPurple} from '../../../constants/Colors';

export interface MembersIconProps {
  size: number;
}

export class MembersIcon extends React.Component<MembersIconProps> {
  render() {
    return <MaterialCommunityIcons name="account-group" size={24} color={secondaryPurple}/>
  }
  }