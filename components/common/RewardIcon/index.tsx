import {Ionicons} from '@expo/vector-icons';
import * as React from 'react';
import {secondaryPurple} from '../../../constants/Colors';

export interface RewardIconProps {
  size: number;
}

export class RewardIcon extends React.Component<RewardIconProps> {
  render() {
    return <Ionicons name="ios-trophy" size={24} color={secondaryPurple} />
  }
  }