import * as React from 'react';
import {secondaryBlue} from '../../../constants/Colors';
import {TabBarIcon} from '../../../navigation/components/TabBarIcon';

export interface SquatIconProps {
  size: number;
}

export class SquatIcon extends React.Component<SquatIconProps> {
  render() {
    return <TabBarIcon name='ios-body' color={secondaryBlue}/>
  }
}