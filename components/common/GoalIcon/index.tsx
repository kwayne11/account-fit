import * as React from 'react';
import {secondaryPurple} from '../../../constants/Colors';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export interface GoalIconProps {
  size: number;
}

export class GoalIcon extends React.Component<GoalIconProps> {
  render() {
    return <MaterialCommunityIcons name="target" size={24} color={secondaryPurple} />
  }
}