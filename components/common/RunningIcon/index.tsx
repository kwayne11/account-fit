import {FontAwesome5} from '@expo/vector-icons';
import * as React from 'react';
import {secondaryGreen} from '../../../constants/Colors';

export interface RunningIconProps {
  size: number;
}

export class RunningIcon extends React.Component<RunningIconProps> {
  render() {
    return <FontAwesome5 name='running' size={30} color={secondaryGreen}/>
  }
}