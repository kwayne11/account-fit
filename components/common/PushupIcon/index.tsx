import * as React from 'react';
import {secondaryPurple} from '../../../constants/Colors';
import {TabBarIcon} from '../../../navigation/components/TabBarIcon';

export interface PushupIconProps {
  size: number;
}

export class PushupIcon extends React.Component<PushupIconProps> {
  render() {
    return <TabBarIcon name='ios-fitness' color={secondaryPurple}/>
  }
}