import * as React from 'react';
import {TextInput} from 'react-native-paper';
import {primaryBlue} from '../../../constants/Colors';

export interface InputTextProps {
  label?: string;
  placeholder?: string;
  mode?: 'flat' | 'outlined' | undefined;
  text?: string;
  dense?: boolean;
  multiline?: boolean;
  onChangeText?: (text: string) => void;
  style?: any;
}

export class InputText extends React.Component<InputTextProps> {
  render() {
    return <TextInput
      style={this.props.style}
      label={this.props.label}
      placeholder={this.props.placeholder}
      mode={this.props.mode}
      dense={this.props.dense}
      multiline={this.props.multiline}
      underlineColor={primaryBlue}
      placeholderTextColor={primaryBlue}
      theme={{colors: {primary: primaryBlue}}}
      value={this.props.text}
      onChangeText={this.props.onChangeText}
    />
  }
}