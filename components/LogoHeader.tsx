import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {Badge, IconButton} from 'react-native-paper';
import {primaryBlue} from '../constants/Colors';
import {GroupModel} from '../models/entities/GroupModel';
import {View} from './Themed';

export interface LogoHeaderProps {
  invitations: GroupModel[];
  onInvitedClick: () => void;
}

export class LogoHeader extends React.Component<LogoHeaderProps> {
  render() {
    return <View style={styles.container}>
      <Image
        style={styles.logo}
        source={require('../assets/images/home_logo.png')}
      />
      <View style={styles.actions}>
        <IconButton icon="inbox" color={primaryBlue} size={28} onPress={this.props.onInvitedClick}/>
        <Badge style={styles.badge} visible={this.props.invitations.length > 0}>{this.props.invitations.length}</Badge>
      </View>
    </View>;
  }
}

const styles = StyleSheet.create({
  logo: {
    width: 180,
    height: 75,
  },
  actions: {
    marginRight: 25,
    display: 'flex',
    flexDirection: 'row',
  },
  badge: {
    marginTop: 5,
    marginLeft: -25,
    alignSelf: 'flex-start',
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  }
});