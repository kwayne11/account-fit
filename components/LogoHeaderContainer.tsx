import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {GroupApiActions} from '../actions/data/GroupApiActions';
import {UiModalActions} from '../actions/ui/UiModalActions';
import {UiModalSetVisibilityActionModel} from '../models/actions/UiModalSetVisibilityActionModel';
import {GroupModel} from '../models/entities/GroupModel';
import {UserModel} from '../models/entities/UserModel';
import {RootState} from '../models/state/RootState';
import {omit} from '../utils';
import {LogoHeader} from './LogoHeader';

export interface LogoHeaderContainerProps {
  componentKey?: string;
  user?: UserModel;
  invitations? : GroupModel[];
  uiModalActions?: UiModalActions;
  groupApiActions?: GroupApiActions;
}

@connect(
  (state: RootState): Partial<LogoHeaderContainerProps> => ({
    user: state.data.userId.user,
    invitations: state.data.invitationGroups.groups || [],
  }),
  (dispatch: Dispatch): Partial<LogoHeaderContainerProps> => ({
    uiModalActions: bindActionCreators(omit(UiModalActions, 'Type'), dispatch),
    groupApiActions: bindActionCreators(omit(GroupApiActions, 'Type'), dispatch),
  })
)
export class LogoHeaderContainer extends React.Component<LogoHeaderContainerProps> {
  render() {
    return <LogoHeader invitations={this.props.invitations!} onInvitedClick={this.onInvitedClick.bind(this)}/>;
  }

  onInvitedClick(): void {
    this.props.uiModalActions!.setVisible(
      {
        key: this.props.componentKey,
        isVisible: true,
      } as UiModalSetVisibilityActionModel
    )
  }

  componentDidMount() {
    this.props.groupApiActions!.loadUserInvitationsRequested(this.props.user!);
  }
}