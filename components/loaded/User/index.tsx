import React from 'react';
import {connect} from 'react-redux';
import {UserModel} from '../../../models/entities/UserModel';
import {RootState} from '../../../models/state/RootState';
import {SplashLoading} from '../../../screens/Splash/components/SplashLoading';

export interface UserLoadedContainerProps {
  user?: UserModel;
  isLoading?: boolean;
  isLoaded?: boolean;
}

@connect(
  (state: RootState): Partial<UserLoadedContainerProps> => ({
    user: state.data.userId.user,
    isLoaded: state.data.userId.isLoaded,
    isLoading: state.data.userId.isLoading,
  }),
  null,
)
export class UserLoadedContainer extends React.Component<UserLoadedContainerProps> {
  render() {
    if (this.props.user && this.props.isLoaded && !this.props.isLoading) {
      return this.props.children;
    } else {
      return <SplashLoading/>;
    }
  }
}