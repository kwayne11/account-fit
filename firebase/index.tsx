import * as firebase from 'firebase';

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyAkUZXPJbdXsMBfnHlWxXI3CZ3lGl_njY8",
  authDomain: "account-fit.firebaseapp.com",
  databaseURL: "https://account-fit.firebaseio.com",
  projectId: "account-fit",
  storageBucket: "account-fit.appspot.com",
  messagingSenderId: "842549564391",
  appId: "1:842549564391:web:3d9c9e580eb3f6628f2342",
  measurementId: "G-RTPLW35RGK"
};

firebase.initializeApp(firebaseConfig);
export const firestoreInstance: firebase.firestore.Firestore = firebase.firestore();
