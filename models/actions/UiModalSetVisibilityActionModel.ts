export interface UiModalSetVisibilityActionModel {
  key: string;
  isVisible: boolean;
}