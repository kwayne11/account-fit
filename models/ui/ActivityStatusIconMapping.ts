import {ActivityStatus} from '../data/ActivityStatus';

export interface ActivityStatusDisplayModel {
  icon: string;
  color: string;
  text: string;
}

export const ActivityStatusIconMapping: {[status: string]: ActivityStatusDisplayModel} = {
  [ActivityStatus.NOT_STARTED]: {icon: 'progress-empty', color: 'red', text: 'Not Started'},
  [ActivityStatus.IN_PROGRESS]: {icon: 'progress-one', color: 'orange', text: 'In Progress'},
  [ActivityStatus.COMPLETED]: {icon: 'progress-full', color: 'green', text: 'Completed'},
}