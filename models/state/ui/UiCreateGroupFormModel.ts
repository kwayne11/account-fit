import {ExerciseOptionModel} from '../../entities/ExerciseOptionModel';

export interface UiCreateGroupFormModel {
  name: string;
  reward: string;
  memberSearch: string;
  invited: string[];
  options: ExerciseOptionModel[];
  selectedActivity: string;
  selectedTrackingType: string;
  repetitions: string;
  distance: string;
  time: string;
  daysRequired: string;
}