import {UiCreateGroupFormModel} from './UiCreateGroupFormModel';
import {UiModalModel} from './UiModalModel';

export type UiModalState = { [key: string]: UiModalModel };

export interface UiModel {
  createGroupForm: UiCreateGroupFormModel;
  modal: UiModalState;
}