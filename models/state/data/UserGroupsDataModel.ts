import {GroupModel} from '../../entities/GroupModel';
import {ILoadable} from './ILoadable';

export interface UserGroupsDataModel extends ILoadable {
  groups?: GroupModel[];
}