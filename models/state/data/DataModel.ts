import {InvitationGroupsDataModel} from './InvitationGroupsDataModel';
import {UserGroupsDataModel} from './UserGroupsDataModel';
import {UserIdDataModel} from './UserIdDataModel';

export interface DataModel {
    invitationGroups: InvitationGroupsDataModel;
    userId: UserIdDataModel;
    userGroups: UserGroupsDataModel;
}