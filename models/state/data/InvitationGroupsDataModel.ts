import {GroupModel} from '../../entities/GroupModel';
import {ILoadable} from './ILoadable';

export interface InvitationGroupsDataModel extends ILoadable {
  groups?: GroupModel[];
}