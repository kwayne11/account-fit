export interface ILoadable {
    isLoading: boolean;
    isLoaded: boolean;
}