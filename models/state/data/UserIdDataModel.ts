import {UserModel} from '../../entities/UserModel';
import {ILoadable} from './ILoadable';

export interface UserIdDataModel extends ILoadable {
    user?: UserModel;
}