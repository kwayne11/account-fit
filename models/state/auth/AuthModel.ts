export interface LoginResult {
  type?: 'success';
  accessToken?: string | null;
  idToken?: string | null;
  refreshToken?: string | null;
  user: GoogleUser | AppleUser;
}

export interface GoogleUser {
  id?: string;
  name?: string;
  givenName?: string;
  familyName?: string;
  photoUrl?: string;
  email?: string;
}

export interface AppleUser {
  identityToken?: string;
  name?: string;
  givenName?: string;
  familyName?: string;
  email?: string;
}

export interface AuthModel {
  isAuthenticated: boolean;
  isLoaded: boolean;
  loginResult: LoginResult | undefined;
}