import {EnvironmentModel} from '../../lib/models/EnvironmentModel';
import {AuthModel} from './auth/AuthModel';
import {DataModel} from './data/DataModel';
import {UiModel} from './ui/UiModel';

export interface RootState {
  router?: any;
  environment: RootState.EnvironmentState;
  auth: RootState.AuthState;
  ui: RootState.UiState;
  data: RootState.DataState;
}

export namespace RootState {
  export type EnvironmentState = EnvironmentModel;
  export type AuthState = AuthModel;
  export type UiState = UiModel;
  export type DataState = DataModel;
}