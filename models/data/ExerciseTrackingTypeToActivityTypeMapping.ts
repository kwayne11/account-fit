import {ExerciseActivityType} from '../entities/ExerciseActivityType';
import {ExerciseTrackingType} from '../entities/ExerciseTrackingType';

export const ExerciseTrackingTypeToActivityType: { [type: string]: ExerciseActivityType[] } = {
  [ExerciseTrackingType.DISTANCE]: [
    ExerciseActivityType.CYCLING,
    ExerciseActivityType.ELLIPTICAL,
    ExerciseActivityType.RUN,
    ExerciseActivityType.WALK,
  ],
  [ExerciseTrackingType.REPETITIONS]: [
    ExerciseActivityType.SIT_UPS,
    ExerciseActivityType.SQUATS,
    ExerciseActivityType.PUSH_UPS,
    ExerciseActivityType.PULL_UPS,
    ExerciseActivityType.KETTLEBELL_SWINGS,
  ],
  [ExerciseTrackingType.TIME]: [
    ExerciseActivityType.CYCLING,
    ExerciseActivityType.ELLIPTICAL,
    ExerciseActivityType.RUN,
    ExerciseActivityType.WALK,
    ExerciseActivityType.YOGA,
    ExerciseActivityType.STRENGTH_TRAINING,
    ExerciseActivityType.HIGH_INTENSITY_INTERVAL_TRAINING,
    ExerciseActivityType.CROSS_TRAINING,
    ExerciseActivityType.CORE_TRAINING,
  ]
}
