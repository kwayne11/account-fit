import {ExerciseActivityType} from '../entities/ExerciseActivityType';

export interface ExerciseActivityTypeDisplayModel {
  displayText: string;
  icon: string;
}

export const ExerciseActivityTypeDisplayMapping: { [type: string]: ExerciseActivityTypeDisplayModel } = {
  [ExerciseActivityType.CORE_TRAINING]: {
    displayText: 'Core Training',
    icon: 'heart-pulse',
  },
  [ExerciseActivityType.CROSS_TRAINING]: {
    displayText: 'Cross Training',
    icon: 'heart-pulse',
  },
  [ExerciseActivityType.CYCLING]: {
    displayText: 'Cycling',
    icon: 'bike',
  },
  [ExerciseActivityType.ELLIPTICAL]: {
    displayText: 'Elliptical',
    icon: 'heart-pulse',
  },
  [ExerciseActivityType.HIGH_INTENSITY_INTERVAL_TRAINING]: {
    displayText: 'High Intensity Interval Training',
    icon: 'heart-pulse',
  },
  [ExerciseActivityType.KETTLEBELL_SWINGS]: {
    displayText: 'Kettlebell Swings',
    icon: 'weight',
  },
  [ExerciseActivityType.PULL_UPS]: {
    displayText: 'Pull Ups',
    icon: 'human-handsup',
  },
  [ExerciseActivityType.PUSH_UPS]: {
    displayText: 'Push Ups',
    icon: 'heart-pulse',
  },
  [ExerciseActivityType.RUN]: {
    displayText: 'Run',
    icon: 'run-fast',
  },
  [ExerciseActivityType.SIT_UPS]: {
    displayText: 'Sit Ups',
    icon: 'heart-pulse',
  },
  [ExerciseActivityType.SQUATS]: {
    displayText: 'Squats',
    icon: 'human-handsdown',
  },
  [ExerciseActivityType.STRENGTH_TRAINING]: {
    displayText: 'Strength Training',
    icon: 'weight',
  },
  [ExerciseActivityType.WALK]: {
    displayText: 'Walk',
    icon: 'walk',
  },
  [ExerciseActivityType.YOGA]: {
    displayText: 'Yoga',
    icon: 'brain',
  },
}