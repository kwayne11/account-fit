import {ExerciseTrackingType} from '../entities/ExerciseTrackingType';

export interface ExerciseTrackingTypeDisplayModel {
  displayText: string;
}

export const ExerciseTrackingTypeDisplayMapping: { [type: string]: ExerciseTrackingTypeDisplayModel } = {
  [ExerciseTrackingType.DISTANCE]: {
    displayText: 'Distance',
  },
  [ExerciseTrackingType.REPETITIONS]: {
    displayText: 'Repetitions',
  },
  [ExerciseTrackingType.TIME]: {
    displayText: 'Time',
  },
}