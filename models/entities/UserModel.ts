export interface UserModel {
  uid: string;
  name: string;
  email: string;
  createdAt: string;
  updatedAt: string;
}