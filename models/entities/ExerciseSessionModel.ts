import {ExerciseSessionStateModel} from './ExerciseSessionStateModel';
import {GroupModel} from './GroupModel';
import {UserModel} from './UserModel';

export interface ExerciseSessionModel {
  id: string;
  externalId: string;
  user: UserModel;
  group: GroupModel;
  state: ExerciseSessionStateModel;
  updatedAt: string;
}