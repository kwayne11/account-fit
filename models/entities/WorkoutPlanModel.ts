import {ExerciseOptionModel} from './ExerciseOptionModel';

export interface WorkoutPlanModel {
  uid: string;
  groupId: string;
  reward: string;
  exerciseOptions: ExerciseOptionModel[];
  startDate: string;
  endDate: string;
  daysRequired: number;
}
