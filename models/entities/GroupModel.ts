export interface GroupModel {
  uid: string;
  gid?: string;
  name: string;
  invited: string[];
  members: string[];
  createdAt: string;
  updatedAt: string;
}