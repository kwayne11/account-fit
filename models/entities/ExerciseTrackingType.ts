export enum ExerciseTrackingType {
  REPETITIONS = 'REPETITIONS',
  TIME = 'TIME',
  DISTANCE = 'DISTANCE',
}