import {ExerciseActivityType} from './ExerciseActivityType';
import {ExerciseTrackingType} from './ExerciseTrackingType';

export interface ExerciseOptionModel {
  uid: string;
  exerciseActivity: ExerciseActivityType;
  exerciseTrackingType: ExerciseTrackingType;
  minutes?: number | null;
  repetitions?: number | null;
  distance?: number | null;
}