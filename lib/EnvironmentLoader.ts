import axios, {AxiosResponse} from 'axios';

export class EnvironmentLoader {
    public async load(): Promise<EnvironmentLoader> {
        const response: AxiosResponse = await axios.get('/assets/environment.json');
        return {...response.data, isLoaded: true};
    }
}