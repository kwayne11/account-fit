import {MyClass} from "./MyClass";

describe('MyClass', () => {
  describe('add()', () => {
    it('adds two numbers', () => {
      expect(MyClass.add(1, 2)).toEqual(3);
    });
  });
});