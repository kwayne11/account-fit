import {AppleAuthenticationCredential} from 'expo-apple-authentication/src/AppleAuthentication.types';
import {LogInResult} from 'expo-google-app-auth';
import * as SecureStore from 'expo-secure-store';
import firebase from 'firebase';
import {userIdTokenSecureStoreKey} from '../../constants/SecureStore';
import {nonce} from './AppleAuthenticationApi';

export class FirebaseAuthenticationApi {
  async signInWithApple(credential: AppleAuthenticationCredential): Promise<firebase.auth.UserCredential> {
    const provider = new firebase.auth.OAuthProvider('apple.com');
    provider.addScope('email');
    provider.addScope('name');
    const fbCredential = provider.credential({
      idToken: credential.identityToken!,
      rawNonce: nonce,
    });
    return firebase.auth().signInWithCredential(fbCredential);
  }

  async signInWithGoogle(credential: LogInResult): Promise<firebase.auth.UserCredential> {
    if (credential.type !== "cancel") {
      let accessToken: string;
      if (firebase.auth().currentUser !== null) {
        accessToken = credential.accessToken!;
      } else {
        accessToken = credential.accessToken!;
      }
      await SecureStore.setItemAsync(userIdTokenSecureStoreKey, accessToken, {keychainAccessible: SecureStore.ALWAYS});
      const fbCredential: firebase.auth.OAuthCredential = firebase.auth.GoogleAuthProvider.credential(null, accessToken);
      return firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
        .then(() => {
          return firebase.auth().signInWithCredential(fbCredential)
        });
    }
    throw Error('error signing in with Google to firebase');
  }

  async updateDisplayNameForAppleUser(credential: AppleAuthenticationCredential): Promise<void> {
    const currentUser: firebase.User | null = firebase.auth().currentUser;
    console.log('Current User: ');
    console.log(currentUser);
    return currentUser?.updateProfile({displayName: `${credential.fullName!.givenName} ${credential.fullName!.familyName}`})
  }

  async signOutOfFirebase(): Promise<void> {
    await SecureStore.deleteItemAsync(userIdTokenSecureStoreKey);
    return firebase.auth().signOut();
  }
}