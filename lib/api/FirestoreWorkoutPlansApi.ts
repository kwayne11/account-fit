import {firestoreInstance} from '../../firebase';
import {GroupModel} from '../../models/entities/GroupModel';
import {WorkoutPlanModel} from '../../models/entities/WorkoutPlanModel';

export class FirestoreWorkoutPlansApi {
  async addNewWorkoutPlan(plan: WorkoutPlanModel): Promise<void> {
    await firestoreInstance.collection('workout_plans').doc(plan.uid).set(plan);
  }

  async loadWorkoutPlansForUser(groups: GroupModel[]): Promise<any> {
    const groupsIds: string[] = groups.map((group: GroupModel): string => group.uid);
    const querySnapshot = await firestoreInstance.collectionGroup('workout_plans').where('groupId', 'in', groupsIds).get();
    return querySnapshot.docs.map((doc: any) => doc.data());
  }
}