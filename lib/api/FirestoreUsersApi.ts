import {User} from 'firebase';
import {firestoreInstance} from '../../firebase';
import {UserModel} from '../../models/entities/UserModel';

export class FirestoreUsersApi {
  async addNewUser(user: UserModel): Promise<void> {
    await firestoreInstance.collection('users').doc(user.uid).set(user);
  }

  async loadUser(user: User): Promise<any> {
    const doc = await firestoreInstance.collection('users').doc(user.uid).get();
    return doc.data();
  }
}