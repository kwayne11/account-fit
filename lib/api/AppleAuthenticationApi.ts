import * as AppleAuthentication from 'expo-apple-authentication';
import {AppleAuthenticationCredential} from 'expo-apple-authentication/src/AppleAuthentication.types';
import * as Crypto from 'expo-crypto';

export const nonce = Math.random().toString(36).substring(2, 10);

export class AppleAuthenticationApi {
  async signInWithApple(): Promise<AppleAuthenticationCredential> {
    const csrf = Math.random().toString(36).substring(2, 15);
    const hashedNonce = await Crypto.digestStringAsync(
      Crypto.CryptoDigestAlgorithm.SHA256, nonce);
    return AppleAuthentication.signInAsync({
      requestedScopes: [
        AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
        AppleAuthentication.AppleAuthenticationScope.EMAIL,
      ],
      state: csrf,
      nonce: hashedNonce
    });
  }
}