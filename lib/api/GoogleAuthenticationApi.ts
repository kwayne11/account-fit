import * as Google from 'expo-google-app-auth';

export class GoogleAuthenticationApi {
  async signInWithGoogle(): Promise<Google.LogInResult> {
    return Google.logInAsync({
      iosStandaloneAppClientId: '672092350254-b2gi6f6lo7vft7sb1dmsf7qqo1uskqe6.apps.googleusercontent.com',
      iosClientId: '672092350254-ajv1v0d4posc1cp66fh30r8qoha9s1sr.apps.googleusercontent.com',
      scopes: ['profile', 'email'],
    });
  }
}