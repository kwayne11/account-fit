import {firestoreInstance} from '../../firebase';
import {GroupModel} from '../../models/entities/GroupModel';
import {UserModel} from '../../models/entities/UserModel';

export class FirestoreGroupsApi {
  async addNewGroup(group: GroupModel): Promise<void> {
    await firestoreInstance.collection('groups').doc(group.uid).set(group);
  }

  async loadGroupsForUser(user: UserModel): Promise<any> {
    const querySnapshot = await firestoreInstance.collectionGroup('groups').where('members', 'array-contains', user.email).get();
    return querySnapshot.docs.map((doc: any) => doc.data());
  }

  async loadGroupInvitations(user: UserModel): Promise<any> {
    const querySnapshot = await firestoreInstance.collectionGroup('groups').where('invited', 'array-contains', user.email).get();
    return querySnapshot.docs.map((doc: any) => ({...doc.data(), gid: doc.id} as GroupModel));
  }

  async acceptInvitation(user: UserModel, group: GroupModel): Promise<void> {
    const groupRef = firestoreInstance.collection('groups').doc(group.gid);

    try {
      await firestoreInstance.runTransaction(async (t) => {
        const doc = await t.get(groupRef);
        const groupDoc: GroupModel = doc.data() as GroupModel;
        t.update(groupRef, {
          members: [...groupDoc!.members, user.email],
          invited: groupDoc!.invited.filter((u: string) => user.email !== u),
        } as Partial<GroupModel>);
      });
    } catch (e) {
      return e;
    }
  }

  async rejectInvitation(user: UserModel, group: GroupModel): Promise<void> {
    const groupRef = firestoreInstance.collection('groups').doc(group.gid);

    try {
      await firestoreInstance.runTransaction(async (t) => {
        const doc = await t.get(groupRef);
        const groupDoc: GroupModel = doc.data() as GroupModel;
        t.update(groupRef, {
          invited: groupDoc!.invited.filter((u: string) => user.email !== u),
        } as Partial<GroupModel>);
      });
    } catch (e) {
      return e;
    }
  }
}