import {ExerciseOptionModel} from '../../models/entities/ExerciseOptionModel';
import {ExerciseTrackingType} from '../../models/entities/ExerciseTrackingType';

export class ExerciseOptionDisplay {
  static getDescription(option: ExerciseOptionModel): string {
    if (option.exerciseTrackingType === ExerciseTrackingType.REPETITIONS) {
      return `${option.repetitions} Repetitions`;
    } else if (option.exerciseTrackingType === ExerciseTrackingType.DISTANCE) {
      return `${option.distance} Miles`;
    } else if (option.exerciseTrackingType === ExerciseTrackingType.TIME) {
      return `${option.minutes} Minutes`;
    }
    return '';
  }

}