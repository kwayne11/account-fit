export type RootStackParamList = {
  Splash: undefined;
  SignIn: undefined;
  Root: undefined;
  AccountabilityGroupCreation: undefined;
  NotFound: undefined;
};

export type AccountabilityGroupCreationStackParamList = {
  Welcome: undefined;
  GroupName: undefined;
  WorkoutPlan: undefined;
  Members: undefined;
  End: undefined;
};

export type BottomTabParamList = {
  Home: undefined;
  Profile: undefined;
};

export type HomeTabParamList = {
  HomeScreen: undefined;
};

export type ProfileTabParamList = {
  ProfileScreen: undefined;
  GroupDetailsScreen: undefined;
};
