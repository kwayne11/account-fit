import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import {GroupCreationEndContainer} from '../screens/AccountabilityGroupCreation/components/GroupCreationEndContainer';
import {GroupCreationInfo} from '../screens/AccountabilityGroupCreation/components/GroupCreationInfo';
import {GroupCreationMembers} from '../screens/AccountabilityGroupCreation/components/GroupCreationMembers';
import {GroupCreationStart} from '../screens/AccountabilityGroupCreation/components/GroupCreationStart';
import {GroupCreationWorkoutPlan} from '../screens/AccountabilityGroupCreation/components/GroupCreationWorkoutPlan';
import {AccountabilityGroupCreationStackParamList} from '../types';

const Stack = createStackNavigator<AccountabilityGroupCreationStackParamList>();

export class AccountabilityGroupCreationNavigator extends React.Component {
  render() {
    return (
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Welcome" component={GroupCreationStart}/>
        <Stack.Screen name="GroupName" component={GroupCreationInfo}/>
        <Stack.Screen name="WorkoutPlan" component={GroupCreationWorkoutPlan}/>
        <Stack.Screen name="Members" component={GroupCreationMembers}/>
        <Stack.Screen name="End" component={GroupCreationEndContainer}/>
      </Stack.Navigator>
    );
  }
}
