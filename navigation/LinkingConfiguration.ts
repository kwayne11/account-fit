import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Splash: {
        screens: {
          Splash: 'splash'
        }
      },
      SignIn: {
        screens: {
          SignInScreen: 'sign-in'
        }
      },
      Root: {
        screens: {
          Home: {
            screens: {
              HomeScreen: 'home',
            },
          },
          Profile: {
            screens: {
              ProfileScreen: 'profile',
              GroupDetailsScreen: 'group-details',
            },
          },
        },
      },
      AccountabilityGroupCreation: {
        screens: {
          AccountabilityGroupCreation: 'accountability-group-creation'
        }
      },
      NotFound: '*',
    },
  },
};
