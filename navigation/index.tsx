import {DarkTheme, DefaultTheme, NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import {ColorSchemeName} from 'react-native';
import {connect} from 'react-redux';
import {UserModel} from '../models/entities/UserModel';
import {RootState} from '../models/state/RootState';
import NotFoundScreen from '../screens/NotFoundScreen';
import SignInScreen from '../screens/SignIn';
import {SplashScreen} from '../screens/Splash';
import {RootStackParamList} from '../types';
import {AccountabilityGroupCreationNavigator} from './AccountabilityGroupCreationNavigator';
import BottomTabNavigator from './BottomTabNavigator';
import LinkingConfiguration from './LinkingConfiguration';

// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
export default function Navigation({colorScheme}: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootNavigator/>
    </NavigationContainer>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

export interface RootNavigatorProps {
  isLoading?: boolean;
  user?: UserModel;
}

@connect(
  (state: RootState) => ({
    isLoading: state.data.userId.isLoading,
    user: state.data.userId.user,
  }),
  null,
)
export class RootNavigator extends React.Component<RootNavigatorProps> {
  constructor(props: RootNavigatorProps) {
    super(props);
  }

  render() {
    if (this.props.isLoading && !this.props.user) {
      return <></>;
    }
    return (
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Splash" component={SplashScreen}/>
        <Stack.Screen name="SignIn" component={SignInScreen}/>
        <Stack.Screen name="Root" component={BottomTabNavigator}/>
        <Stack.Screen name="AccountabilityGroupCreation" component={AccountabilityGroupCreationNavigator}/>
        <Stack.Screen name="NotFound" component={NotFoundScreen} options={{title: 'Oops!'}}/>
      </Stack.Navigator>
    );
  }
}
