import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import {GroupDetailsScreen} from '../screens/GroupDetails';
import {ProfileScreen} from '../screens/Profile';
import {ProfileTabParamList} from '../types';

export const ProfileTabStack = createStackNavigator<ProfileTabParamList>();

export function ProfileTabNavigator() {
  return (
    <ProfileTabStack.Navigator>
      <ProfileTabStack.Screen
        name="ProfileScreen"
        component={ProfileScreen}
        options={{headerShown: false}}
      />
      <ProfileTabStack.Screen
        name="GroupDetailsScreen"
        component={GroupDetailsScreen}
        options={{headerShown: false}}
      />
    </ProfileTabStack.Navigator>
  );
}
