import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import {HomeScreen} from '../screens/Home';
import {HomeTabParamList} from '../types';

export const HomeTabStack = createStackNavigator<HomeTabParamList>();

export function HomeTabNavigator() {
  return (
    <HomeTabStack.Navigator>
      <HomeTabStack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{headerShown: false}}
      />
    </HomeTabStack.Navigator>
  );
}
