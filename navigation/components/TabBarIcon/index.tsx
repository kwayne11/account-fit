import {Ionicons} from '@expo/vector-icons';
import React from 'react';

export interface TabBarIconProps {
  name: string;
  color: string;
  style?: any;
}

export class TabBarIcon extends React.Component<TabBarIconProps> {
  render() {
    return <Ionicons size={30} {...this.props}/>;
  }
}