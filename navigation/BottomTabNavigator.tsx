import {Feather} from '@expo/vector-icons';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import * as React from 'react';
import {StyleSheet} from 'react-native';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import {BottomTabParamList} from '../types';
import {TabBarIcon} from './components/TabBarIcon';
import {HomeTabNavigator} from './HomeTabNavigator';
import {ProfileTabNavigator} from './ProfileTabNavigator';


const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      tabBarOptions={{activeTintColor: Colors[colorScheme].tint, showLabel: false}}>
      <BottomTab.Screen
        name="Home"
        component={HomeTabNavigator}
        options={{
          tabBarIcon: ({color}) => <Feather name="activity" size={30} color={color}
                                            style={styles.icon}/>,
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={ProfileTabNavigator}
        options={{
          tabBarIcon: ({color}) => <TabBarIcon name="ios-people" color={color} style={styles.icon}/>,
        }}
      />
    </BottomTab.Navigator>
  );
}

const styles: any = StyleSheet.create({
  icon: {
    marginBottom: -3,
  },
});