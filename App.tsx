import {StatusBar} from 'expo-status-bar';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/firestore';
import 'firebase/storage';
import React from 'react';
import {AppearanceProvider} from 'react-native-appearance';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {Theme} from 'react-native-paper/lib/typescript/src/types';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'react-redux';
import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import {SplashLoading} from './screens/Splash/components/SplashLoading';
import {configureStore} from './store';

const theme: Theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#f2f2f2',
    accent: '#293d58',
  },
};

const store = configureStore();

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (!isLoadingComplete) {
    return <SplashLoading/>;
  } else {
    return <SafeAreaProvider>
      <AppearanceProvider>
        <PaperProvider theme={theme}>
          <Provider store={store}>
            <Navigation colorScheme={colorScheme}/>
          </Provider>
          <StatusBar/>
        </PaperProvider>
      </AppearanceProvider>
    </SafeAreaProvider>;
  }
}
