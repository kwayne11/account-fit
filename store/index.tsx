import {applyMiddleware, createStore, Store} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import thunk from 'redux-thunk';
import {logger} from '../middleware/logger';
import rootSaga from '../middleware/sagas';
import {RootState} from '../models/state/RootState';
import {createRootReducer} from '../reducers';

export function configureStore(initialState?: RootState): Store<RootState> {
  const sagaMiddleware = createSagaMiddleware();
  let middleware = applyMiddleware(logger, sagaMiddleware, thunk);

  middleware = composeWithDevTools({trace: true, traceLimit: 25})(middleware);

  const store = createStore(createRootReducer(), initialState as any, middleware) as Store<RootState>;
  sagaMiddleware.run(rootSaga);

  return store;
}