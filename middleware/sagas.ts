import {all} from 'redux-saga/effects';
import {AppleAuthenticationApiActions} from '../actions/data/AppleAuthenticationApiActions';
import {EnvironmentActions} from '../actions/data/EnvironmentActions';
import {FirebaseAuthenticationApiActions} from '../actions/data/FirebaseAuthenticationApiActions';
import {GoogleAuthenticationApiActions} from '../actions/data/GoogleAuthenticationApiActions';
import {GroupApiActions} from '../actions/data/GroupApiActions';
import {WorkoutPlanApiActions} from '../actions/data/WorkoutPlanApiActions';
import {UiCreateGroupFormActions} from '../actions/ui/UiCreateGroupFormActions';

export default function* rootSaga() {
  yield all([
    AppleAuthenticationApiActions.watch(),
    EnvironmentActions.watchLoadEnvironmentConfig(),
    FirebaseAuthenticationApiActions.watch(),
    GroupApiActions.watch(),
    GoogleAuthenticationApiActions.watch(),
    UiCreateGroupFormActions.watch(),
    WorkoutPlanApiActions.watch(),
  ]);
}