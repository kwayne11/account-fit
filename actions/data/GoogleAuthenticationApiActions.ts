import {all, call, put, takeEvery} from '@redux-saga/core/effects';
import {LogInResult} from 'expo-google-app-auth';
import {createAction} from 'redux-actions';
import {GoogleAuthenticationApi} from '../../lib/api/GoogleAuthenticationApi';
import {FirebaseAuthenticationApiActions} from './FirebaseAuthenticationApiActions';

export const googleAuthenticationApi: GoogleAuthenticationApi = new GoogleAuthenticationApi();

export namespace GoogleAuthenticationApiActions {
  export enum Type {
    GOOGLE_SIGN_IN_REQUESTED = 'GOOGLE_SIGN_IN_REQUESTED',
    GOOGLE_SIGN_IN_SUCCEEDED = 'GOOGLE_SIGN_IN_SUCCEEDED',
    GOOGLE_SIGN_IN_FAILED = 'GOOGLE_SIGN_IN_FAILED',
  }

  export function* watch() {
    yield takeEvery(GoogleAuthenticationApiActions.Type.GOOGLE_SIGN_IN_REQUESTED, signInUserId);
  }

  export function* signInUserId() {
    try {
      const result: LogInResult = yield call(googleAuthenticationApi.signInWithGoogle);
      if (result.type === 'success') {
        yield all([
          put(signInSucceeded(result)),
          put(FirebaseAuthenticationApiActions.signInWithGoogleRequested(result)),
        ]);
      } else {
        yield put(signInFailed(new Error('Error signing in with Google')))
      }
    } catch (error) {
      yield put(signInFailed(error))
    }
  }

  export const signInRequested = createAction(Type.GOOGLE_SIGN_IN_REQUESTED);
  export const signInSucceeded = createAction<LogInResult>(Type.GOOGLE_SIGN_IN_SUCCEEDED);
  export const signInFailed = createAction<Error>(Type.GOOGLE_SIGN_IN_FAILED);
}

export type GoogleAuthenticationApiActions = Omit<typeof GoogleAuthenticationApiActions, 'Type'>;
