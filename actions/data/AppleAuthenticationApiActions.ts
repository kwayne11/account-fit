import {all, call, put, takeEvery} from '@redux-saga/core/effects';
import {AppleAuthenticationCredential} from 'expo-apple-authentication';
import {createAction} from 'redux-actions';
import {AppleAuthenticationApi} from '../../lib/api/AppleAuthenticationApi';
import {FirebaseAuthenticationApi} from '../../lib/api/FirebaseAuthenticationApi';
import {LoginResult} from '../../models/state/auth/AuthModel';
import {FirebaseAuthenticationApiActions} from './FirebaseAuthenticationApiActions';

export const appleAuthenticationApi: AppleAuthenticationApi = new AppleAuthenticationApi();
export const firebaseAuthenticationApi: FirebaseAuthenticationApi = new FirebaseAuthenticationApi();

export namespace AppleAuthenticationApiActions {
  export enum Type {
    APPLE_SIGN_IN_REQUESTED = 'APPLE_SIGN_IN_REQUESTED',
    APPLE_SIGN_IN_SUCCEEDED = 'APPLE_SIGN_IN_SUCCEEDED',
    APPLE_SIGN_IN_FAILED = 'APPLE_SIGN_IN_FAILED',
  }

  export function* watch() {
    yield takeEvery(AppleAuthenticationApiActions.Type.APPLE_SIGN_IN_REQUESTED, signInUserId);
  }

  export function* signInUserId() {
    try {
      const credential: AppleAuthenticationCredential = yield call(appleAuthenticationApi.signInWithApple);
      if (credential) {
        yield all([
          yield put(signInSucceeded({
            type: 'success', user: {
              givenName: credential.fullName!.givenName,
              familyName: credential.fullName!.familyName,
              email: credential.email,
              identityToken: credential.identityToken,
            }
          } as LoginResult)),
          put(FirebaseAuthenticationApiActions.signInWithAppleRequested(credential)),
        ]);
      } else {
        yield put(signInFailed(new Error('Error signing in with Apple')))
      }
    } catch (error) {
      yield put(signInFailed(error))
    }
  }

  export const signInRequested = createAction(Type.APPLE_SIGN_IN_REQUESTED);
  export const signInSucceeded = createAction<LoginResult>(Type.APPLE_SIGN_IN_SUCCEEDED);
  export const signInFailed = createAction<Error>(Type.APPLE_SIGN_IN_FAILED);
}

export type AppleAuthenticationApiActions = Omit<typeof AppleAuthenticationApiActions, 'Type'>;
