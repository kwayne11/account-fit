import {all, call, put, takeEvery} from '@redux-saga/core/effects';
import {Action, createAction} from 'redux-actions';
import {FirestoreWorkoutPlansApi} from '../../lib/api/FirestoreWorkoutPlansApi';
import {GroupModel} from '../../models/entities/GroupModel';
import {WorkoutPlanModel} from '../../models/entities/WorkoutPlanModel';

export const firestoreWorkoutPlansApi: FirestoreWorkoutPlansApi = new FirestoreWorkoutPlansApi();

export namespace WorkoutPlanApiActions {
  export enum Type {
    FIREBASE_ADD_NEW_WORKOUT_PLAN_REQUESTED = 'FIREBASE_ADD_NEW_WORKOUT_PLAN_REQUESTED',
    FIREBASE_ADD_NEW_WORKOUT_PLAN_SUCCEEDED = 'FIREBASE_ADD_NEW_WORKOUT_PLAN_SUCCEEDED',
    FIREBASE_ADD_NEW_WORKOUT_PLAN_FAILED = 'FIREBASE_ADD_NEW_WORKOUT_PLAN_FAILED',

    FIREBASE_LOAD_USER_WORKOUT_PLANS_REQUESTED = 'FIREBASE_LOAD_USER_WORKOUT_PLANS_REQUESTED',
    FIREBASE_LOAD_USER_WORKOUT_PLANS_SUCCEEDED = 'FIREBASE_LOAD_USER_WORKOUT_PLANS_SUCCEEDED',
    FIREBASE_LOAD_USER_WORKOUT_PLANS_FAILED = 'FIREBASE_LOAD_USER_WORKOUT_PLANS_FAILED',
  }

  export function* addNewWorkoutPlan(action: Action<WorkoutPlanModel>) {
    try {
      yield call(firestoreWorkoutPlansApi.addNewWorkoutPlan, action.payload!);
      yield put(addNewWorkoutPlanSucceeded());
    } catch (error) {
      yield put(addNewWorkoutPlanFailed(error))
    }
  }

  export function* loadUsersWorkoutPlans(action: Action<GroupModel[]>) {
    try {
      const workoutPlans: WorkoutPlanModel[] = yield call(firestoreWorkoutPlansApi.loadWorkoutPlansForUser, action.payload!);
      yield put(loadUserWorkoutPlansSucceeded(workoutPlans));
    } catch (error) {
      yield put(loadUserWorkoutPlansFailed(error))
    }
  }

  export function* watch() {
    yield all([
      yield takeEvery(WorkoutPlanApiActions.Type.FIREBASE_ADD_NEW_WORKOUT_PLAN_REQUESTED, addNewWorkoutPlan),
      yield takeEvery(WorkoutPlanApiActions.Type.FIREBASE_LOAD_USER_WORKOUT_PLANS_REQUESTED, loadUsersWorkoutPlans),
    ]);
  }

  export const addNewWorkoutPlanRequested = createAction<WorkoutPlanModel>(Type.FIREBASE_ADD_NEW_WORKOUT_PLAN_REQUESTED);
  export const addNewWorkoutPlanSucceeded = createAction(Type.FIREBASE_ADD_NEW_WORKOUT_PLAN_SUCCEEDED);
  export const addNewWorkoutPlanFailed = createAction<Error>(Type.FIREBASE_ADD_NEW_WORKOUT_PLAN_FAILED);

  export const loadUserWorkoutPlansRequested = createAction<GroupModel[]>(Type.FIREBASE_LOAD_USER_WORKOUT_PLANS_REQUESTED);
  export const loadUserWorkoutPlansSucceeded = createAction<WorkoutPlanModel[]>(Type.FIREBASE_LOAD_USER_WORKOUT_PLANS_SUCCEEDED);
  export const loadUserWorkoutPlansFailed = createAction<Error>(Type.FIREBASE_LOAD_USER_WORKOUT_PLANS_FAILED);
}

export type WorkoutPlanApiActions = Omit<typeof WorkoutPlanApiActions, 'Type'>;
