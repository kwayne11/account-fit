import {all, call, put, select, takeEvery} from '@redux-saga/core/effects';
import {Action, createAction} from 'redux-actions';
import {FirestoreGroupsApi} from '../../lib/api/FirestoreGroupsApi';
import {GroupModel} from '../../models/entities/GroupModel';
import {UserModel} from '../../models/entities/UserModel';
import {RootState} from '../../models/state/RootState';

export const firestoreGroupsApi: FirestoreGroupsApi = new FirestoreGroupsApi();

export namespace GroupApiActions {
  export enum Type {
    FIREBASE_ADD_NEW_GROUP_REQUESTED = 'FIREBASE_ADD_NEW_GROUP_REQUESTED',
    FIREBASE_ADD_NEW_GROUP_SUCCEEDED = 'FIREBASE_ADD_NEW_GROUP_SUCCEEDED',
    FIREBASE_ADD_NEW_GROUP_FAILED = 'FIREBASE_ADD_NEW_GROUP_FAILED',

    FIREBASE_LOAD_USER_GROUPS_REQUESTED = 'FIREBASE_LOAD_USER_GROUPS_REQUESTED',
    FIREBASE_LOAD_USER_GROUPS_SUCCEEDED = 'FIREBASE_LOAD_USER_GROUPS_SUCCEEDED',
    FIREBASE_LOAD_USER_GROUPS_FAILED = 'FIREBASE_LOAD_USER_GROUPS_FAILED',

    FIREBASE_LOAD_USER_INVITATIONS_REQUESTED = 'FIREBASE_LOAD_USER_INVITATIONS_REQUESTED',
    FIREBASE_LOAD_USER_INVITATIONS_SUCCEEDED = 'FIREBASE_LOAD_USER_INVITATIONS_SUCCEEDED',
    FIREBASE_LOAD_USER_INVITATIONS_FAILED = 'FIREBASE_LOAD_USER_INVITATIONS_FAILED',

    FIREBASE_ACCEPT_GROUP_INVITATION_REQUESTED = 'FIREBASE_ACCEPT_GROUP_INVITATION_REQUESTED',
    FIREBASE_ACCEPT_GROUP_INVITATION_SUCCEEDED = 'FIREBASE_ACCEPT_GROUP_INVITATION_SUCCEEDED',
    FIREBASE_ACCEPT_GROUP_INVITATION_FAILED = 'FIREBASE_ACCEPT_GROUP_INVITATION_FAILED',

    FIREBASE_REJECT_GROUP_INVITATION_REQUESTED = 'FIREBASE_REJECT_GROUP_INVITATION_REQUESTED',
    FIREBASE_REJECT_GROUP_INVITATION_SUCCEEDED = 'FIREBASE_REJECT_GROUP_INVITATION_SUCCEEDED',
    FIREBASE_REJECT_GROUP_INVITATION_FAILED = 'FIREBASE_REJECT_GROUP_INVITATION_FAILED',
  }

  function getUserId(state: RootState) {
    return state.data.userId.user;
  };

  export function* addNewGroup(action: Action<GroupModel>) {
    try {
      yield call(firestoreGroupsApi.addNewGroup, action.payload!);
      yield put(addNewGroupSucceeded());
      const user: UserModel = yield select(getUserId);
      yield put(loadUserGroupsRequested(user));
    } catch (error) {
      yield put(addNewGroupFailed(error))
    }
  }

  export function* loadUserInvitations(action: Action<UserModel>) {
    try {
      const groups: GroupModel[] = yield call(firestoreGroupsApi.loadGroupInvitations, action.payload!);
      yield put(loadUserInvitationsSucceeded(groups));
    } catch (error) {
      yield put(loadUserInvitationsFailed(error))
    }
  }

  export function* loadUserGroups(action: Action<UserModel>) {
    try {
      const groups: GroupModel[] = yield call(firestoreGroupsApi.loadGroupsForUser, action.payload!);
      yield put(loadUserGroupsSucceeded(groups));
    } catch (error) {
      yield put(loadUserGroupsFailed(error))
    }
  }

  export function* acceptInvitation(action: Action<GroupModel>) {
    try {
      const user: UserModel = yield select(getUserId);
      yield call(firestoreGroupsApi.acceptInvitation, user, action.payload!);
      yield put(acceptGroupInvitationSucceeded());
      yield put(loadUserInvitationsRequested(user));
      yield put(loadUserGroupsRequested(user));
    } catch (error) {
      console.log(error);
      yield put(acceptGroupInvitationFailed(error))
    }
  }

  export function* rejectInvitation(action: Action<GroupModel>) {
    try {
      const user: UserModel = yield select(getUserId);
      yield call(firestoreGroupsApi.rejectInvitation, user, action.payload!);
      yield put(rejectGroupInvitationSucceeded());
      yield put(loadUserInvitationsRequested(user));
      yield put(loadUserGroupsRequested(user));
    } catch (error) {
      console.log(error);
      yield put(rejectGroupInvitationFailed(error))
    }
  }

  export function* watch() {
    yield all([
      yield takeEvery(GroupApiActions.Type.FIREBASE_ADD_NEW_GROUP_REQUESTED, addNewGroup),
      yield takeEvery(GroupApiActions.Type.FIREBASE_LOAD_USER_INVITATIONS_REQUESTED, loadUserInvitations),
      yield takeEvery(GroupApiActions.Type.FIREBASE_LOAD_USER_GROUPS_REQUESTED, loadUserGroups),
      yield takeEvery(GroupApiActions.Type.FIREBASE_ACCEPT_GROUP_INVITATION_REQUESTED, acceptInvitation),
      yield takeEvery(GroupApiActions.Type.FIREBASE_REJECT_GROUP_INVITATION_REQUESTED, rejectInvitation),
    ]);
  }

  export const addNewGroupRequested = createAction<GroupModel>(Type.FIREBASE_ADD_NEW_GROUP_REQUESTED);
  export const addNewGroupSucceeded = createAction(Type.FIREBASE_ADD_NEW_GROUP_SUCCEEDED);
  export const addNewGroupFailed = createAction<Error>(Type.FIREBASE_ADD_NEW_GROUP_FAILED);

  export const loadUserGroupsRequested = createAction<UserModel>(Type.FIREBASE_LOAD_USER_GROUPS_REQUESTED);
  export const loadUserGroupsSucceeded = createAction<GroupModel[]>(Type.FIREBASE_LOAD_USER_GROUPS_SUCCEEDED);
  export const loadUserGroupsFailed = createAction<Error>(Type.FIREBASE_LOAD_USER_GROUPS_FAILED);

  export const loadUserInvitationsRequested = createAction<UserModel>(Type.FIREBASE_LOAD_USER_INVITATIONS_REQUESTED);
  export const loadUserInvitationsSucceeded = createAction<GroupModel[]>(Type.FIREBASE_LOAD_USER_INVITATIONS_SUCCEEDED);
  export const loadUserInvitationsFailed = createAction<Error>(Type.FIREBASE_LOAD_USER_INVITATIONS_FAILED);

  export const acceptGroupInvitationRequested = createAction<GroupModel>(Type.FIREBASE_ACCEPT_GROUP_INVITATION_REQUESTED);
  export const acceptGroupInvitationSucceeded = createAction(Type.FIREBASE_ACCEPT_GROUP_INVITATION_SUCCEEDED);
  export const acceptGroupInvitationFailed = createAction<Error>(Type.FIREBASE_ACCEPT_GROUP_INVITATION_FAILED);

  export const rejectGroupInvitationRequested = createAction<GroupModel>(Type.FIREBASE_REJECT_GROUP_INVITATION_REQUESTED);
  export const rejectGroupInvitationSucceeded = createAction(Type.FIREBASE_REJECT_GROUP_INVITATION_SUCCEEDED);
  export const rejectGroupInvitationFailed = createAction<Error>(Type.FIREBASE_REJECT_GROUP_INVITATION_FAILED);
}

export type GroupApiActions = Omit<typeof GroupApiActions, 'Type'>;
