import {all, call, put, takeEvery,} from '@redux-saga/core/effects';
import {AppleAuthenticationCredential} from 'expo-apple-authentication';
import {LogInResult} from 'expo-google-app-auth';
import firebase, {User} from 'firebase';
import {Action, createAction} from 'redux-actions';
import {take} from 'redux-saga/effects';
import {FirebaseAuthenticationApi} from '../../lib/api/FirebaseAuthenticationApi';
import {FirestoreUsersApi} from '../../lib/api/FirestoreUsersApi';
import {UserModel} from '../../models/entities/UserModel';

export const firebaseAuthenticationApi: FirebaseAuthenticationApi = new FirebaseAuthenticationApi();
export const firestoreUsersApi: FirestoreUsersApi = new FirestoreUsersApi();

export namespace FirebaseAuthenticationApiActions {
  export enum Type {
    FIREBASE_ADD_NEW_USER_REQUESTED = 'FIREBASE_ADD_NEW_USER_REQUESTED',
    FIREBASE_ADD_NEW_USER_SUCCEEDED = 'FIREBASE_ADD_NEW_USER_SUCCEEDED',
    FIREBASE_ADD_NEW_USER_FAILED = 'FIREBASE_ADD_NEW_USER_FAILED',

    FIREBASE_SIGN_IN_WITH_GOOGLE_REQUESTED = 'FIREBASE_SIGN_IN_WITH_GOOGLE_REQUESTED',
    FIREBASE_SIGN_IN_WITH_GOOGLE_SUCCEEDED = 'FIREBASE_SIGN_IN_WITH_GOOGLE_SUCCEEDED',
    FIREBASE_SIGN_IN_WITH_GOOGLE_FAILED = 'FIREBASE_SIGN_IN_WITH_GOOGLE_FAILED',

    FIREBASE_SIGN_IN_WITH_APPLE_REQUESTED = 'FIREBASE_SIGN_IN_WITH_APPLE_REQUESTED',
    FIREBASE_SIGN_IN_WITH_APPLE_SUCCEEDED = 'FIREBASE_SIGN_IN_WITH_APPLE_SUCCEEDED',
    FIREBASE_SIGN_IN_WITH_APPLE_FAILED = 'FIREBASE_SIGN_IN_WITH_APPLE_FAILED',

    FIREBASE_SIGN_OUT_REQUESTED = 'FIREBASE_SIGN_OUT_REQUESTED',
    FIREBASE_SIGN_OUT_SUCCEEDED = 'FIREBASE_SIGN_OUT_SUCCEEDED',
    FIREBASE_SIGN_OUT_FAILED = 'FIREBASE_SIGN_OUT_FAILED',

    FIREBASE_LOAD_USER_REQUESTED = 'FIREBASE_LOAD_USER_REQUESTED',
    FIREBASE_LOAD_USER_SUCCEEDED = 'FIREBASE_LOAD_USER_SUCCEEDED',
    FIREBASE_LOAD_USER_FAILED = 'FIREBASE_LOAD_USER_FAILED',
  }

  export function* addNewFirebaseUser(action: Action<UserModel>) {
    try {
      yield call(firestoreUsersApi.addNewUser, action.payload!);
      yield put(addNewUserSucceeded());
    } catch (error) {
      yield put(addNewUserFailed(error))
    }
  }

  export function* signInFirebaseUserWithGoogle(action: Action<LogInResult>) {
    try {
      const credential: firebase.auth.UserCredential = yield call(firebaseAuthenticationApi.signInWithGoogle, action.payload!);
      if (credential) {
        if (credential.additionalUserInfo?.isNewUser) {
          yield put(addNewUserRequested({
            uid: credential.user?.uid,
            name: credential.user?.displayName,
            email: credential.user?.email,
            createdAt: new Date().toUTCString(),
            updatedAt: new Date().toUTCString(),
          } as UserModel));
        }
        yield put(signInWithGoogleSucceeded(credential.user!));
      } else {
        yield put(signInWithGoogleFailed(new Error('Error signing in user to firebase via google.')))
      }
    } catch (error) {
      yield put(signInWithGoogleFailed(error))
    }
  }

  export function* signInFirebaseUserWithApple(action: Action<AppleAuthenticationCredential>) {
    try {
      const result: AppleAuthenticationCredential | null = action.payload!;
      const credential: firebase.auth.UserCredential = yield call(firebaseAuthenticationApi.signInWithApple, action.payload!);
      yield call(firebaseAuthenticationApi.updateDisplayNameForAppleUser, result);
      if (credential?.additionalUserInfo?.isNewUser) {
        yield put(addNewUserRequested({
          uid: credential.user?.uid,
          name: `${result.fullName!.givenName} ${result.fullName!.familyName}`,
          email: credential.user?.email,
          createdAt: new Date().toUTCString(),
          updatedAt: new Date().toUTCString(),
        } as UserModel));
        yield take(addNewUserSucceeded);
        yield put(signInWithAppleSucceeded(credential.user!));
      } else {
        yield put(signInWithAppleFailed(new Error('Error signing in user to firebase via apple.')))
      }
    } catch (error) {
      yield put(signInWithAppleFailed(error))
    }
  }

  export function* signOutFirebaseUser() {
    try {
      yield call(firebaseAuthenticationApi.signOutOfFirebase);
      yield put(signOutSucceeded(undefined));
    } catch (error) {
      yield put(signOutFailed(error))
    }
  }

  export function* loadFirebaseUser(action: Action<User>) {
    try {
      const userModel: UserModel = yield call(firestoreUsersApi.loadUser, action.payload!);
      yield put(loadUserSucceeded(userModel));
    } catch (error) {
      yield put(loadUserFailed(error))
    }
  }

  export function* watch() {
    yield all([
      yield takeEvery(FirebaseAuthenticationApiActions.Type.FIREBASE_ADD_NEW_USER_REQUESTED, addNewFirebaseUser),
      yield takeEvery(FirebaseAuthenticationApiActions.Type.FIREBASE_SIGN_OUT_REQUESTED, signOutFirebaseUser),
      yield takeEvery(FirebaseAuthenticationApiActions.Type.FIREBASE_SIGN_IN_WITH_APPLE_REQUESTED, signInFirebaseUserWithApple),
      yield takeEvery(FirebaseAuthenticationApiActions.Type.FIREBASE_SIGN_IN_WITH_GOOGLE_REQUESTED, signInFirebaseUserWithGoogle),
      yield takeEvery(FirebaseAuthenticationApiActions.Type.FIREBASE_LOAD_USER_REQUESTED, loadFirebaseUser),
    ]);
  }

  export const addNewUserRequested = createAction(Type.FIREBASE_ADD_NEW_USER_REQUESTED);
  export const addNewUserSucceeded = createAction(Type.FIREBASE_ADD_NEW_USER_SUCCEEDED);
  export const addNewUserFailed = createAction(Type.FIREBASE_ADD_NEW_USER_FAILED);

  export const signInWithGoogleRequested = createAction<LogInResult>(Type.FIREBASE_SIGN_IN_WITH_GOOGLE_REQUESTED);
  export const signInWithGoogleSucceeded = createAction<User>(Type.FIREBASE_SIGN_IN_WITH_GOOGLE_SUCCEEDED);
  export const signInWithGoogleFailed = createAction<Error>(Type.FIREBASE_SIGN_IN_WITH_GOOGLE_FAILED);

  export const signInWithAppleRequested = createAction<AppleAuthenticationCredential>(Type.FIREBASE_SIGN_IN_WITH_APPLE_REQUESTED);
  export const signInWithAppleSucceeded = createAction<User>(Type.FIREBASE_SIGN_IN_WITH_APPLE_SUCCEEDED);
  export const signInWithAppleFailed = createAction<Error>(Type.FIREBASE_SIGN_IN_WITH_APPLE_FAILED);

  export const signOutRequested = createAction(Type.FIREBASE_SIGN_OUT_REQUESTED);
  export const signOutSucceeded = createAction<undefined>(Type.FIREBASE_SIGN_OUT_SUCCEEDED);
  export const signOutFailed = createAction<Error>(Type.FIREBASE_SIGN_OUT_FAILED);

  export const loadUserRequested = createAction<User>(Type.FIREBASE_LOAD_USER_REQUESTED);
  export const loadUserSucceeded = createAction<UserModel>(Type.FIREBASE_LOAD_USER_SUCCEEDED);
  export const loadUserFailed = createAction<Error>(Type.FIREBASE_LOAD_USER_FAILED);
}

export type FirebaseAuthenticationApiActions = Omit<typeof FirebaseAuthenticationApiActions, 'Type'>;
