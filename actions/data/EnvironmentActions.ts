import {call, put, takeLatest} from '@redux-saga/core/effects';
import {createAction} from 'redux-actions';
import {EnvironmentLoader} from '../../lib/EnvironmentLoader';
import {EnvironmentModel} from '../../lib/models/EnvironmentModel';

export const environmentLoader: EnvironmentLoader = new EnvironmentLoader();

export namespace EnvironmentActions {
  export enum Type {
    ENVIRONMENT_LOAD_REQUESTED = 'ENVIRONMENT_LOAD_REQUESTED',
    ENVIRONMENT_LOAD_SUCCEEDED = 'ENVIRONMENT_LOAD_SUCCEEDED',
    ENVIRONMENT_LOAD_FAILED = 'ENVIRONMENT_LOAD_FAILED',
  }

  export function* loadEnvironmentConfig() {
    try {
      yield put(EnvironmentActions.loadEnvironmentConfigSucceeded(yield call(environmentLoader.load)));
    } catch (error) {
      yield put(EnvironmentActions.loadEnvironmentConfigFailed(error));
    }
  }

  export function* watchLoadEnvironmentConfig() {
    yield takeLatest(EnvironmentActions.Type.ENVIRONMENT_LOAD_REQUESTED, loadEnvironmentConfig);
  }

  export const loadEnvironmentConfigRequested = createAction(Type.ENVIRONMENT_LOAD_REQUESTED);
  export const loadEnvironmentConfigSucceeded = createAction<EnvironmentModel>(Type.ENVIRONMENT_LOAD_SUCCEEDED);
  export const loadEnvironmentConfigFailed = createAction<Error>(Type.ENVIRONMENT_LOAD_FAILED);
}

export type EnvironmentActions = Omit<typeof EnvironmentActions, 'Type'>;