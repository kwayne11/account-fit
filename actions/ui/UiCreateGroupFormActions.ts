import {all, put, select, take, takeEvery} from '@redux-saga/core/effects';
import {createAction} from 'redux-actions';
import {v4 as uuid} from 'uuid';
import {ExerciseOptionModel} from '../../models/entities/ExerciseOptionModel';
import {GroupModel} from '../../models/entities/GroupModel';
import {UserModel} from '../../models/entities/UserModel';
import {WorkoutPlanModel} from '../../models/entities/WorkoutPlanModel';
import {RootState} from '../../models/state/RootState';
import {UiCreateGroupFormModel} from '../../models/state/ui/UiCreateGroupFormModel';
import {GroupApiActions} from '../data/GroupApiActions';
import {WorkoutPlanApiActions} from '../data/WorkoutPlanApiActions';

export namespace UiCreateGroupFormActions {
  export enum Type {
    CREATE_GROUP_FORM_ADD_OPTION = 'CREATE_GROUP_FORM_ADD_OPTION',
    CREATE_GROUP_FORM_REMOVE_OPTION = 'CREATE_GROUP_FORM_REMOVE_OPTION',
    CREATE_GROUP_FORM_EDIT_NAME = 'CREATE_GROUP_FORM_EDIT_NAME',
    CREATE_GROUP_FORM_EDIT_REWARD = 'CREATE_GROUP_FORM_EDIT_REWARD',
    CREATE_GROUP_FORM_ADD_MEMBER = 'CREATE_GROUP_FORM_ADD_MEMBER',
    CREATE_GROUP_FORM_REMOVE_MEMBER = 'CREATE_GROUP_FORM_REMOVE_MEMBER',
    CREATE_GROUP_FORM_EDIT_MEMBER_SEARCH = 'CREATE_GROUP_FORM_EDIT_MEMBER_SEARCH',
    CREATE_GROUP_FORM_SET_SELECTED_ACTIVITY = 'CREATE_GROUP_FORM_SET_SELECTED_ACTIVITY',
    CREATE_GROUP_FORM_SET_SELECTED_TRACKING_TYPE = 'CREATE_GROUP_FORM_SET_SELECTED_TRACKING_TYPE',
    CREATE_GROUP_FORM_SET_TIME = 'CREATE_GROUP_FORM_SET_TIME',
    CREATE_GROUP_FORM_SET_DISTANCE = 'CREATE_GROUP_FORM_SET_DISTANCE',
    CREATE_GROUP_FORM_SET_REPETITIONS = 'CREATE_GROUP_FORM_SET_REPETITIONS',
    CREATE_GROUP_FORM_SET_DAYS_REQUIRED = 'CREATE_GROUP_FORM_SET_DAYS_REQUIRED',
    CREATE_GROUP_FORM_CREATE_GROUP_WITH_WORKOUT = 'CREATE_GROUP_FORM_CREATE_GROUP_WITH_WORKOUT',
    CREATE_GROUP_FORM_RESET_FORM = 'CREATE_GROUP_FORM_RESET_FORM',
  }

  function getGroup(state: RootState): UiCreateGroupFormModel {
    return state.ui.createGroupForm
  };

  function getUserId(state: RootState): UserModel {
    return state.data.userId.user!;
  };

  function getUserGroups(state: RootState): GroupModel[] {
    return state.data.userGroups.groups!;
  }

  export function* addNewGroupWithWorkoutPlan() {
    try {
      const groupForm: UiCreateGroupFormModel = yield select(getGroup);
      const user: UserModel = yield select(getUserId);
      const groupId = uuid();
      const currentDate: Date = new Date();
      const endDate: Date = new Date(new Date().setDate(currentDate.getDate() + 30));
      yield put(GroupApiActions.addNewGroupRequested({
        uid: groupId,
        name: groupForm.name,
        invited: groupForm.invited,
        members: [user.email],
        createdAt: currentDate.toUTCString(),
        updatedAt: currentDate.toUTCString(),
      } as GroupModel));
      yield take(GroupApiActions.addNewGroupSucceeded);
      yield put(WorkoutPlanApiActions.addNewWorkoutPlanRequested({
        uid: uuid(),
        groupId: groupId,
        reward: groupForm.reward,
        exerciseOptions: groupForm.options,
        daysRequired: +groupForm.daysRequired,
        startDate: currentDate.toUTCString(),
        endDate: endDate.toUTCString(),
      } as WorkoutPlanModel))
      yield take(WorkoutPlanApiActions.addNewWorkoutPlanSucceeded);
      yield put(GroupApiActions.loadUserGroupsRequested(user));
      yield take(GroupApiActions.loadUserGroupsSucceeded);
      const groups: GroupModel[] = yield select(getUserGroups);
      yield put(WorkoutPlanApiActions.loadUserWorkoutPlansRequested(groups));
      yield put(UiCreateGroupFormActions.resetForm());
    } catch (error) {
      yield put(GroupApiActions.addNewGroupFailed(error))
    }
  }

  export function* watch() {
    yield all([
      yield takeEvery(UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_CREATE_GROUP_WITH_WORKOUT, addNewGroupWithWorkoutPlan),
    ]);
  }

  export const addOption = createAction<ExerciseOptionModel>(Type.CREATE_GROUP_FORM_ADD_OPTION);
  export const removeOption = createAction<string>(Type.CREATE_GROUP_FORM_REMOVE_OPTION);
  export const addMember = createAction<string>(Type.CREATE_GROUP_FORM_ADD_MEMBER);
  export const removeMember = createAction<string>(Type.CREATE_GROUP_FORM_REMOVE_MEMBER);
  export const editMemberSearch = createAction<string>(Type.CREATE_GROUP_FORM_EDIT_MEMBER_SEARCH);
  export const editName = createAction<string>(Type.CREATE_GROUP_FORM_EDIT_NAME);
  export const editReward = createAction<string>(Type.CREATE_GROUP_FORM_EDIT_REWARD);
  export const setSelectedActivity = createAction<string>(Type.CREATE_GROUP_FORM_SET_SELECTED_ACTIVITY);
  export const setSelectedTrackingType = createAction<string>(Type.CREATE_GROUP_FORM_SET_SELECTED_TRACKING_TYPE);
  export const setTimeInput = createAction<string>(Type.CREATE_GROUP_FORM_SET_TIME);
  export const setDistanceInput = createAction<string>(Type.CREATE_GROUP_FORM_SET_DISTANCE);
  export const setRepetitionsInput = createAction<string>(Type.CREATE_GROUP_FORM_SET_REPETITIONS);
  export const setDaysRequired = createAction<string>(Type.CREATE_GROUP_FORM_SET_DAYS_REQUIRED);
  export const createGroupWithWorkout = createAction(Type.CREATE_GROUP_FORM_CREATE_GROUP_WITH_WORKOUT);
  export const resetForm = createAction(Type.CREATE_GROUP_FORM_RESET_FORM);
}

export type UiCreateGroupFormActions = Omit<typeof UiCreateGroupFormActions, 'Type'>;
