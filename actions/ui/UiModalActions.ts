import {createAction} from 'redux-actions';
import {UiModalSetVisibilityActionModel} from '../../models/actions/UiModalSetVisibilityActionModel';

export namespace UiModalActions {
  export enum Type {
    MODAL_SET_VISIBLE = 'MODAL_SET_VISIBLE',
  }

  export const setVisible = createAction<UiModalSetVisibilityActionModel>(Type.MODAL_SET_VISIBLE);
}

export type UiModalActions = Omit<typeof UiModalActions, 'Type'>;
