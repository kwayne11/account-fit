import * as React from 'react';
import {Searchbar} from 'react-native-paper';

export interface GroupCreationInfoMemberSearchProps {
  text: string;
  onChange: (value: string) => void;
  style?: any;
}

export class GroupCreationInfoMemberSearch extends React.Component<GroupCreationInfoMemberSearchProps> {
  render() {
    return <Searchbar
      autoCapitalize="none"
      placeholder="Email Search"
      onChangeText={this.props.onChange}
      value={this.props.text}
    />
  }
}