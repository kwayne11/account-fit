import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {UiCreateGroupFormActions} from '../../../../actions/ui/UiCreateGroupFormActions';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';
import {GroupCreationInfoGroupNameInput} from '../GroupCreationInfoGroupNameInput';

export interface GroupCreationInfoGroupNameInputContainerProps {
  name?: string;
  style?: any;
  uiCreateGroupFormActions?: UiCreateGroupFormActions;
}

@connect(
  (state: RootState): Partial<GroupCreationInfoGroupNameInputContainerProps> => ({
    name: state.ui.createGroupForm.name,
  }),
  (dispatch: Dispatch): Partial<GroupCreationInfoGroupNameInputContainerProps> => ({
    uiCreateGroupFormActions: bindActionCreators(omit(UiCreateGroupFormActions, 'Type'), dispatch),
  })
)
export class GroupCreationInfoGroupNameInputContainer extends React.Component<GroupCreationInfoGroupNameInputContainerProps> {
  render() {
    return <GroupCreationInfoGroupNameInput style={this.props.style} text={this.props.name || ''}
                                            onChange={this.onChange.bind(this)}/>;
  }

  onChange(value: string): void {
    this.props.uiCreateGroupFormActions!.editName(value);
  }
}