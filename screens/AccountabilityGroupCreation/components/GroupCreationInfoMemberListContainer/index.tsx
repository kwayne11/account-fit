import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {UiCreateGroupFormActions} from '../../../../actions/ui/UiCreateGroupFormActions';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';
import {GroupCreationInfoMemberList} from '../GroupCreationInfoMemberList';

export interface GroupCreationInfoMemberListContainerProps {
  invited?: string[];
  uiCreateGroupFormActions?: UiCreateGroupFormActions;
}

@connect(
  (state: RootState): Partial<GroupCreationInfoMemberListContainerProps> => ({
    invited: state.ui.createGroupForm.invited,
  }),
  (dispatch: Dispatch): Partial<GroupCreationInfoMemberListContainerProps> => ({
    uiCreateGroupFormActions: bindActionCreators(omit(UiCreateGroupFormActions, 'Type'), dispatch),
  })
)
export class GroupCreationInfoMemberListContainer extends React.Component<GroupCreationInfoMemberListContainerProps> {
  render() {
    return <GroupCreationInfoMemberList invited={this.props.invited!} onClose={this.onClose.bind(this)}/>;
  }

  onClose(value: string): void {
    this.props.uiCreateGroupFormActions!.removeMember(value);
  }
}