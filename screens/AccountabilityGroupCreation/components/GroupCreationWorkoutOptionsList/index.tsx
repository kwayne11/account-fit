import * as React from 'react';
import {StyleSheet} from 'react-native';
import {IconButton, List, Subheading, Title} from 'react-native-paper';
import {darkGrey, primaryBlue} from '../../../../constants/Colors';
import {ExerciseOptionDisplay} from '../../../../lib/display/ExerciseOptionDisplay';
import {ExerciseActivityTypeDisplayMapping} from '../../../../models/data/ExerciseActivityTypeDisplayMapping';
import {ExerciseOptionModel} from '../../../../models/entities/ExerciseOptionModel';

export interface GroupCreationWorkoutOptionsListProps {
  options: ExerciseOptionModel[];
  onDelete: (uid: string) => void;
}

export class GroupCreationWorkoutOptionsList extends React.Component<GroupCreationWorkoutOptionsListProps> {
  render() {
    return (
      <>
        <Subheading style={styles.subheading}>Group Workout Options</Subheading>
        <List.Section>
          {this.props.options.map((option: ExerciseOptionModel) => {
            return <List.Item key={option.uid}
                              title={ExerciseActivityTypeDisplayMapping[option.exerciseActivity].displayText}
                              description={ExerciseOptionDisplay.getDescription(option)}
                              left={() => {
                                return <List.Icon
                                  icon={ExerciseActivityTypeDisplayMapping[option.exerciseActivity].icon}/>
                              }}
                              right={() => <>
                                <IconButton color='#F43366' icon="delete"
                                            onPress={() => this.props.onDelete(option.uid)}/>
                              </>}/>
          })}
        </List.Section>
      </>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    color: primaryBlue,
    textAlign: 'center',
  },
  subheading: {
    color: darkGrey,
    marginTop: 24,
  },
});