import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {UiCreateGroupFormActions} from '../../../../actions/ui/UiCreateGroupFormActions';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';
import {GroupCreationInfoGroupRewardInput} from '../GroupCreationInfoGroupRewardInput';

export interface GroupCreationInfoGroupRewardInputContainerProps {
  reward?: string;
  style?: any;
  uiCreateGroupFormActions?: UiCreateGroupFormActions;
}

@connect(
  (state: RootState): Partial<GroupCreationInfoGroupRewardInputContainerProps> => ({
    reward: state.ui.createGroupForm.reward,
  }),
  (dispatch: Dispatch): Partial<GroupCreationInfoGroupRewardInputContainerProps> => ({
    uiCreateGroupFormActions: bindActionCreators(omit(UiCreateGroupFormActions, 'Type'), dispatch),
  })
)
export class GroupCreationInfoGroupRewardInputContainer extends React.Component<GroupCreationInfoGroupRewardInputContainerProps> {
  render() {
    return <GroupCreationInfoGroupRewardInput style={this.props.style} reward={this.props.reward!} onChange={this.onChange.bind(this)}/>;
  }

  onChange(value: string): void {
    this.props.uiCreateGroupFormActions!.editReward(value);
  }
}