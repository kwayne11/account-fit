import * as React from 'react';
import {StyleSheet} from 'react-native';
import {Chip, Subheading} from 'react-native-paper';
import {View} from '../../../../components/Themed';
import {darkGrey} from '../../../../constants/Colors';

export interface GroupCreationInfoMemberListProps {
  invited: string[];
  onClose: (value: string) => void;
  style?: any;
}

export class GroupCreationInfoMemberList extends React.Component<GroupCreationInfoMemberListProps> {
  render() {
    return <><Subheading style={styles.subheading}>Invited members:</Subheading>
      <View style={styles.container}>
        {this.props.invited.map((invite: string) => {
          return <Chip key={invite} style={styles.chip} icon="email-plus"
                       onClose={() => this.props.onClose(invite)}>{invite}</Chip>
        })}
      </View>
    </>
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 4,
  },
  subheading: {
    color: darkGrey,
    marginTop: 24,
    marginBottom: 16,
  },
});