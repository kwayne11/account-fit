import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {UiCreateGroupFormActions} from '../../../../actions/ui/UiCreateGroupFormActions';
import {AllExerciseActivityTypes} from '../../../../models/entities/ExerciseActivityType';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';
import {GroupCreationWorkoutActivityPicker} from '../GroupCreationWorkoutActivityPicker';

export interface GroupCreationWorkoutActivityPickerContainerProps {
  selectedActivity?: string;
  uiCreateGroupFormActions?: UiCreateGroupFormActions;
}

@connect(
  (state: RootState): Partial<GroupCreationWorkoutActivityPickerContainerProps> => ({
    selectedActivity: state.ui.createGroupForm.selectedActivity,
  }),
  (dispatch: Dispatch): Partial<GroupCreationWorkoutActivityPickerContainerProps> => ({
    uiCreateGroupFormActions: bindActionCreators(omit(UiCreateGroupFormActions, 'Type'), dispatch),
  })
)
export class GroupCreationWorkoutActivityPickerContainer extends React.Component<GroupCreationWorkoutActivityPickerContainerProps> {
  render() {
    return <GroupCreationWorkoutActivityPicker activities={AllExerciseActivityTypes}
                                               selectedActivity={this.props.selectedActivity!}
                                               onSelectionChange={this.onSelectionChange.bind(this)}/>;
  }

  onSelectionChange(selection: string): void {
    this.props.uiCreateGroupFormActions!.setSelectedActivity(selection);
  }
}