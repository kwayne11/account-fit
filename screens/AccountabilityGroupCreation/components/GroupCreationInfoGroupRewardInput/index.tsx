import * as React from 'react';
import {InputText} from '../../../../components/forms/TextInput';

export interface GroupCreationInfoGroupRewardInputProps {
  reward?: string;
  onChange: (value: string) => void;
  style?: any;
}

export class GroupCreationInfoGroupRewardInput extends React.Component<GroupCreationInfoGroupRewardInputProps> {
  render() {
    return <InputText style={this.props.style} placeholder='Ex: "Group dinner at our favorite restaurant!"'
                      multiline={true} mode='flat' text={this.props.reward} onChangeText={this.props.onChange}/>;
  }
}