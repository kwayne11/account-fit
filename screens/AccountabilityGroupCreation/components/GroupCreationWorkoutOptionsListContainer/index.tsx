import * as React from 'react';
import {Subheading} from 'react-native-paper';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {UiCreateGroupFormActions} from '../../../../actions/ui/UiCreateGroupFormActions';
import {ExerciseOptionModel} from '../../../../models/entities/ExerciseOptionModel';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';
import {GroupCreationWorkoutOptionsList} from '../GroupCreationWorkoutOptionsList';

export interface GroupCreationWorkoutOptionsListContainerProps {
  options?: ExerciseOptionModel[];
  uiCreateGroupFormActions?: UiCreateGroupFormActions;
}

@connect(
  (state: RootState): Partial<GroupCreationWorkoutOptionsListContainerProps> => ({
    options: state.ui.createGroupForm.options,
  }),
  (dispatch: Dispatch): Partial<GroupCreationWorkoutOptionsListContainerProps> => ({
    uiCreateGroupFormActions: bindActionCreators(omit(UiCreateGroupFormActions, 'Type'), dispatch),
  })
)
export class GroupCreationWorkoutOptionsListContainer extends React.Component<GroupCreationWorkoutOptionsListContainerProps> {
  render() {
    return this.props.options!.length > 0 ?
      <GroupCreationWorkoutOptionsList options={this.props.options!} onDelete={this.onDelete.bind(this)}/> :
      <Subheading>Create you first workout option below!</Subheading>
  }

  onDelete(uid: string): void {
    this.props.uiCreateGroupFormActions!.removeOption(uid);
  }
}
