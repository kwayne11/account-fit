import * as React from 'react';
import {StyleSheet} from 'react-native';
import {Button} from 'react-native-paper';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {v4 as uuid} from 'uuid';
import {UiCreateGroupFormActions} from '../../../../actions/ui/UiCreateGroupFormActions';
import {backgroundWhite, primaryBlue} from '../../../../constants/Colors';
import {ExerciseActivityType} from '../../../../models/entities/ExerciseActivityType';
import {ExerciseOptionModel} from '../../../../models/entities/ExerciseOptionModel';
import {ExerciseTrackingType} from '../../../../models/entities/ExerciseTrackingType';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';

export interface GroupCreationWorkoutAddOptionContainerProps {
  selectedActivity?: string;
  selectedTrackingType?: string;
  time?: string;
  distance?: string;
  repetitions?: string;

  uiCreateGroupFormActions?: UiCreateGroupFormActions;
}

@connect(
  (state: RootState): Partial<GroupCreationWorkoutAddOptionContainerProps> => ({
    selectedActivity: state.ui.createGroupForm.selectedActivity,
    selectedTrackingType: state.ui.createGroupForm.selectedTrackingType,
    time: state.ui.createGroupForm.time,
    distance: state.ui.createGroupForm.distance,
    repetitions: state.ui.createGroupForm.repetitions,
  }),
  (dispatch: Dispatch): Partial<GroupCreationWorkoutAddOptionContainerProps> => ({
    uiCreateGroupFormActions: bindActionCreators(omit(UiCreateGroupFormActions, 'Type'), dispatch),
  })
)
export class GroupCreationWorkoutAddOptionContainer extends React.Component<GroupCreationWorkoutAddOptionContainerProps> {
  render() {
    return (
      <Button style={styles.button} labelStyle={styles.labelStyle} icon="plus-circle-outline" mode="contained"
              onPress={this.onAddOption.bind(this)}>
        Add Option
      </Button>
    )
  }

  onAddOption(): void {
    const option: ExerciseOptionModel = {
      uid: uuid(),
      exerciseActivity: this.props.selectedActivity! as ExerciseActivityType,
      exerciseTrackingType: this.props.selectedTrackingType! as ExerciseTrackingType,
      repetitions: this.props.selectedTrackingType === ExerciseTrackingType.REPETITIONS ? parseInt(this.props.repetitions!) : null,
      minutes: this.props.selectedTrackingType === ExerciseTrackingType.TIME ? parseInt(this.props.time!) : null,
      distance: this.props.selectedTrackingType === ExerciseTrackingType.DISTANCE ? parseFloat(this.props.distance!) : null,
    }
    this.props.uiCreateGroupFormActions!.addOption(option);
  }
}


const styles = StyleSheet.create({
  button: {
    marginVertical: 24,
    backgroundColor: primaryBlue,
  },
  labelStyle: {
    color: backgroundWhite,
  }
});