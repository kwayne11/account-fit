import {useNavigation} from '@react-navigation/native';
import * as React from 'react';
import {KeyboardAvoidingView, SafeAreaView, ScrollView, StyleSheet} from 'react-native';
import {Appbar, Button, ProgressBar} from 'react-native-paper';
import {View} from '../../../../components/Themed';
import {primaryBlue} from '../../../../constants/Colors';
import {ExerciseActivityType} from '../../../../models/entities/ExerciseActivityType';
import {ExerciseOptionModel} from '../../../../models/entities/ExerciseOptionModel';
import {ExerciseTrackingType} from '../../../../models/entities/ExerciseTrackingType';
import {GroupCreationWorkoutActivityPickerContainer} from '../GroupCreationWorkoutActivityPickerContainer';
import {GroupCreationWorkoutAddOptionContainer} from '../GroupCreationWorkoutAddOption';
import {GroupCreationWorkoutDaysRequiredContainer} from '../GroupCreationWorkoutDaysRequiredContainer';
import {GroupCreationWorkoutOptionsListContainer} from '../GroupCreationWorkoutOptionsListContainer';
import {GroupCreationWorkoutTrackingTypeSelectContainer} from '../GroupCreationWorkoutTrackingTypeSelectContainer';

const options: ExerciseOptionModel[] = [
  {
    uid: '1',
    distance: 2,
    exerciseTrackingType: ExerciseTrackingType.DISTANCE,
    exerciseActivity: ExerciseActivityType.WALK,
  },
  {
    uid: '2',
    minutes: 30,
    exerciseTrackingType: ExerciseTrackingType.TIME,
    exerciseActivity: ExerciseActivityType.RUN,
  },
  {
    uid: '3',
    repetitions: 50,
    exerciseTrackingType: ExerciseTrackingType.REPETITIONS,
    exerciseActivity: ExerciseActivityType.PULL_UPS,
  },
];

export function GroupCreationWorkoutPlan() {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={styles.safeAreaView}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => {
          navigation.navigate('GroupName')
        }}/>
        <Appbar.Content title="Accountability Group" subtitle={'Workout Plan'}/>
      </Appbar.Header>
      <ProgressBar progress={0.5} color={primaryBlue}/>
      <KeyboardAvoidingView behavior='position' style={styles.screenContainer}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <GroupCreationWorkoutOptionsListContainer/>
          <View style={styles.activityPicker}>
            <GroupCreationWorkoutActivityPickerContainer/>
            <GroupCreationWorkoutTrackingTypeSelectContainer/>
            <GroupCreationWorkoutAddOptionContainer/>
            <GroupCreationWorkoutDaysRequiredContainer/>
          </View>
          <Button style={styles.button} labelStyle={styles.buttonText} color={primaryBlue} uppercase={false} mode='text'
                  onPress={() => navigation.navigate('Members')}>Invite Members</Button>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  screenContainer: {
    flex: 1,
    padding: 16,
  },
  activityPicker: {
    // flex: 1,
  },
  button: {
    marginTop: 16,
  },
  buttonText: {
    fontSize: 20,
  },
});