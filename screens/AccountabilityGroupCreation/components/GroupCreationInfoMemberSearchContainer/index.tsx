import * as React from 'react';
import {Button} from 'react-native-paper';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {UiCreateGroupFormActions} from '../../../../actions/ui/UiCreateGroupFormActions';
import {primaryBlue} from '../../../../constants/Colors';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';
import {GroupCreationInfoMemberSearch} from '../GroupCreationInfoMemberSearch';

export interface GroupCreationInfoMemberSearchContainerProps {
  memberSearch?: string;
  style?: any;
  uiCreateGroupFormActions?: UiCreateGroupFormActions;
}

@connect(
  (state: RootState): Partial<GroupCreationInfoMemberSearchContainerProps> => ({
    memberSearch: state.ui.createGroupForm.memberSearch,
  }),
  (dispatch: Dispatch): Partial<GroupCreationInfoMemberSearchContainerProps> => ({
    uiCreateGroupFormActions: bindActionCreators(omit(UiCreateGroupFormActions, 'Type'), dispatch),
  })
)
export class GroupCreationInfoMemberSearchContainer extends React.Component<GroupCreationInfoMemberSearchContainerProps> {
  render() {
    return <>
      <GroupCreationInfoMemberSearch text={this.props.memberSearch || ''} onChange={this.onChange.bind(this)}/>
      <Button color={primaryBlue} uppercase={false} mode='text' onPress={this.onAdd.bind(this)}>Add Member</Button>
    </>;
  }

  onChange(value: string): void {
    this.props.uiCreateGroupFormActions!.editMemberSearch(value);
  }

  onAdd(): void {
    this.props.uiCreateGroupFormActions!.addMember(this.props.memberSearch!);
  }
}