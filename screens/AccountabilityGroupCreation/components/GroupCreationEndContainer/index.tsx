import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {UiCreateGroupFormActions} from '../../../../actions/ui/UiCreateGroupFormActions';
import {ExerciseOptionModel} from '../../../../models/entities/ExerciseOptionModel';
import {UserModel} from '../../../../models/entities/UserModel';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';
import {GroupCreationEnd} from '../GroupCreationEnd';

export interface GroupCreationEndContainerProps {
  user?: UserModel;
  name?: string;
  reward?: string;
  invited?: string[];
  options?: ExerciseOptionModel[];
  daysRequired?: string;
  uiCreateGroupFormActions?: UiCreateGroupFormActions;
}

@connect(
  (state: RootState): Partial<GroupCreationEndContainerProps> => ({
    name: state.ui.createGroupForm.name,
    reward: state.ui.createGroupForm.reward,
    invited: state.ui.createGroupForm.invited,
    options: state.ui.createGroupForm.options,
    daysRequired: state.ui.createGroupForm.daysRequired,
    user: state.data.userId.user,
  }),
  (dispatch: Dispatch): Partial<GroupCreationEndContainerProps> => ({
    uiCreateGroupFormActions: bindActionCreators(omit(UiCreateGroupFormActions, 'Type'), dispatch),
  })
)
export class GroupCreationEndContainer extends React.Component<GroupCreationEndContainerProps> {
  render() {
    return <GroupCreationEnd name={this.props.name!} reward={this.props.reward!} invited={this.props.invited!}
                             options={this.props.options!} daysRequired={this.props.daysRequired!}
                             onDone={this.onDone.bind(this)}/>;
  }

  onDone(): void {
    this.props.uiCreateGroupFormActions!.createGroupWithWorkout();
  }
}