import {useNavigation} from '@react-navigation/native';
import * as React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Appbar, Button, ProgressBar} from 'react-native-paper';
import {View} from '../../../../components/Themed';
import {primaryBlue} from '../../../../constants/Colors';
import {GroupCreationInfoMemberListContainer} from '../GroupCreationInfoMemberListContainer';
import {GroupCreationInfoMemberSearchContainer} from '../GroupCreationInfoMemberSearchContainer';


export function GroupCreationMembers() {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={styles.safeAreaView}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => {
          navigation.navigate('WorkoutPlan')
        }}/>
        <Appbar.Content title="Accountability Group" subtitle={'Members'}/>
      </Appbar.Header>
      <ProgressBar progress={0.75} color={primaryBlue}/>
      <View style={styles.screenContainer}>
        <GroupCreationInfoMemberSearchContainer/>
        <GroupCreationInfoMemberListContainer/>
        <Button style={styles.button} labelStyle={styles.buttonText} color={primaryBlue} uppercase={false} mode='text'
                onPress={() => navigation.navigate('End')}>Done</Button>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  screenContainer: {
    padding: 16,
  },
  button: {
    marginTop: 16,
  },
  buttonText: {
    fontSize: 20,
  },
});