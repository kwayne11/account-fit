import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {UiCreateGroupFormActions} from '../../../../actions/ui/UiCreateGroupFormActions';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';
import {GroupCreationWorkoutDaysRequired} from '../GroupCreationWorkoutDaysRequired';
import {GroupCreationWorkoutTrackingTypeSelect} from '../GroupCreationWorkoutTrackingTypeSelect';

export interface GroupCreationWorkoutDaysRequiredContainerProps {
  daysRequired?: string;
  uiCreateGroupFormActions?: UiCreateGroupFormActions;
}

@connect(
  (state: RootState): Partial<GroupCreationWorkoutDaysRequiredContainerProps> => ({
    daysRequired: state.ui.createGroupForm.daysRequired,
  }),
  (dispatch: Dispatch): Partial<GroupCreationWorkoutDaysRequiredContainerProps> => ({
    uiCreateGroupFormActions: bindActionCreators(omit(UiCreateGroupFormActions, 'Type'), dispatch),
  })
)
export class GroupCreationWorkoutDaysRequiredContainer extends React.Component<GroupCreationWorkoutDaysRequiredContainerProps> {
  render() {
    return <GroupCreationWorkoutDaysRequired daysRequired={this.props.daysRequired!} onDaysRequiredChange={this.onDaysRequiredChange.bind(this)}/>;
  }

  onDaysRequiredChange(selection: string): void {
    this.props.uiCreateGroupFormActions!.setDaysRequired(selection);
  }
}