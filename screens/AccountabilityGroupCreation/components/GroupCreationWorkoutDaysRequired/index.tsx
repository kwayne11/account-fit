import * as React from 'react';
import {StyleSheet, TextInput} from 'react-native';
import {Subheading} from 'react-native-paper';
import {View} from '../../../../components/Themed';
import {darkGrey, primaryBlue} from '../../../../constants/Colors';

export interface GroupCreationWorkoutDaysRequiredProps {
  daysRequired: string;
  onDaysRequiredChange: (input: string) => void;
}

export class GroupCreationWorkoutDaysRequired extends React.Component<GroupCreationWorkoutDaysRequiredProps> {
  render() {
    return (
      <>
        <Subheading style={styles.subheading}>Days required to complete workout plan of the next 30 days: </Subheading>
        <View style={styles.inputs}>
          <>
            <TextInput style={styles.input} keyboardType='number-pad' value={this.props.daysRequired}
                       onChangeText={this.props.onDaysRequiredChange}/>
            <Subheading style={styles.unit}>Days</Subheading>
          </>
        </View>
      </>
    )
  }
}

const styles = StyleSheet.create({
  subheading: {
    color: darkGrey,
    marginTop: 24,
    marginBottom: 16,
  },
  inputs: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    textAlign: 'center',
    height: 35,
    width: 70,
    fontSize: 16,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: primaryBlue,
  },
  unit: {
    marginLeft: 8,
  }
});