import * as React from 'react';
import {InputText} from '../../../../components/forms/TextInput';

export interface GroupCreationInfoGroupNameInputProps {
  text: string;
  onChange: (value: string) => void;
  style?: any;
}

export class GroupCreationInfoGroupNameInput extends React.Component<GroupCreationInfoGroupNameInputProps> {
  render() {
    return <InputText style={this.props.style} placeholder='Ex: "Fit Family"' mode='outlined' text={this.props.text} onChangeText={this.props.onChange}/>;
  }
}