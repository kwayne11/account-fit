import * as React from 'react';
import {StyleSheet} from 'react-native';
import {Chip, Subheading} from 'react-native-paper';
import {View} from '../../../../components/Themed';
import {darkGrey} from '../../../../constants/Colors';
import {ExerciseActivityTypeDisplayMapping} from '../../../../models/data/ExerciseActivityTypeDisplayMapping';
import {ExerciseActivityType} from '../../../../models/entities/ExerciseActivityType';

export interface GroupCreationWorkoutActivityPickerProps {
  activities: ExerciseActivityType[];
  selectedActivity: string;
  onSelectionChange: (activity: string) => void;
}

export class GroupCreationWorkoutActivityPicker extends React.Component<GroupCreationWorkoutActivityPickerProps> {
  render() {
    return (
      <>
        <Subheading style={styles.subheading}>Choose Activity:</Subheading>
        <View style={styles.chips}>

          {this.props.activities.map((activity: ExerciseActivityType) => {
            return <Chip key={activity} style={styles.chip} icon={ExerciseActivityTypeDisplayMapping[activity].icon}
                         selected={this.props.selectedActivity === activity}
                         onPress={() => this.props.onSelectionChange(activity)}>{ExerciseActivityTypeDisplayMapping[activity].displayText}</Chip>
          })}
        </View>
      </>
    )
  }
}

const styles = StyleSheet.create({
  subheading: {
    color: darkGrey,
    marginTop: 24,
    marginBottom: 16,
  },
  chips: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
    flex: 1,
  },
  chip: {
    marginBottom: 8,
  }
});