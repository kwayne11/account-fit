import {useNavigation} from '@react-navigation/native';
import * as React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Appbar, Button, ProgressBar, Subheading} from 'react-native-paper';
import {View} from '../../../../components/Themed';
import {darkGrey, primaryBlue} from '../../../../constants/Colors';
import {GroupCreationInfoGroupNameInput} from '../GroupCreationInfoGroupNameInput';
import {GroupCreationInfoGroupNameInputContainer} from '../GroupCreationInfoGroupNameInputContainer';
import {GroupCreationInfoGroupRewardInput} from '../GroupCreationInfoGroupRewardInput';
import {GroupCreationInfoGroupRewardInputContainer} from '../GroupCreationInfoGroupRewardInputContainer';


export function GroupCreationInfo() {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={styles.safeAreaView}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => {
          navigation.navigate('Welcome')
        }}/>
        <Appbar.Content title="Accountability Group" subtitle={'Information'}/>
      </Appbar.Header>
      <ProgressBar progress={0.25} color={primaryBlue}/>
      <View style={styles.screenContainer}>
        <Subheading style={styles.subheading}>Give your fitness group a name:</Subheading>
        <GroupCreationInfoGroupNameInputContainer style={styles.nameInput}/>
        <Subheading style={styles.subheading}>Define the reward for the group reaching the goal:</Subheading>
        <GroupCreationInfoGroupRewardInputContainer style={styles.rewardInput}/>
        <Button style={styles.button} labelStyle={styles.buttonText} color={primaryBlue} uppercase={false} mode='text'
                onPress={() => navigation.navigate('WorkoutPlan')}>Create Workout Plan</Button>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  screenContainer: {
    flex: 1,
    padding: 16,
  },
  subheading: {
    color: darkGrey,
    marginTop: 24,
  },
  nameInput: {
    marginVertical: 16,
  },
  rewardInput: {
    marginTop: 16,
  },
  button: {
    marginTop: 16,
  },
  buttonText: {
    fontSize: 20,
  },
});