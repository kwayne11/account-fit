import {useNavigation} from '@react-navigation/native';
import * as React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Appbar, Button, Card, Chip, ProgressBar, Subheading} from 'react-native-paper';
import {RewardIcon} from '../../../../components/common/RewardIcon';
import {View} from '../../../../components/Themed';
import {darkGrey, primaryBlue} from '../../../../constants/Colors';
import {ExerciseOptionModel} from '../../../../models/entities/ExerciseOptionModel';

export interface GroupCreationEndProps {
  name: string;
  reward: string;
  invited: string[];
  options: ExerciseOptionModel[];
  daysRequired: string;
  onDone: () => void;
}

export function GroupCreationEnd(props: GroupCreationEndProps) {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={styles.safeAreaView}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => {
          navigation.navigate('Members')
        }}/>
        <Appbar.Content title="Accountability Group" subtitle={'Review Group Details'}/>
      </Appbar.Header>
      <ProgressBar progress={1.0} color={primaryBlue}/>
      <View style={styles.screenContainer}>
        <Card style={styles.card}>
          <Card.Title title={props.name}
                      subtitle={`Reward for completing ${props.daysRequired} of the next 30 days: ${props.reward}`}
                      subtitleNumberOfLines={5}
                      left={() => <RewardIcon size={24}/>}/>
        </Card>
        <Subheading style={styles.subheading}>Invited members:</Subheading>
        <View style={styles.chips}>
          {props.invited.map((member: string) => {
            return <Chip key={member} style={styles.chip} icon={'account'}>{member}</Chip>
          })}
        </View>
        <Button style={styles.button} labelStyle={styles.buttonText} color={primaryBlue} uppercase={false} mode='text'
                onPress={() => {
                  props.onDone();
                  return navigation.navigate('Profile');
                }}>Save Group</Button>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  screenContainer: {
    flex: 1,
    padding: 16,
  },
  card: {
    marginTop: 16,
    paddingVertical: 16,
  },
  button: {
    marginTop: 16,
  },
  buttonText: {
    fontSize: 20,
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  subheading: {
    color: darkGrey,
    marginTop: 24,
    marginBottom: 16,
  },
  chips: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
    flex: 1,
  },
  chip: {
    marginBottom: 8,
  }
});