import {useNavigation} from '@react-navigation/native';
import * as React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Appbar, Button, Card, ProgressBar} from 'react-native-paper';
import {GoalIcon} from '../../../../components/common/GoalIcon';
import {MembersIcon} from '../../../../components/common/MembersIcon';
import {RewardIcon} from '../../../../components/common/RewardIcon';
import {View} from '../../../../components/Themed';
import {primaryBlue} from '../../../../constants/Colors';

export interface GroupCreationStartProps {
  navigation?: any;
}

export function GroupCreationStart() {
  const navigation = useNavigation();

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => {
          navigation.navigate('Profile')
        }}/>
        <Appbar.Content title="Accountability Group" subtitle={'Start'} />
      </Appbar.Header>
      <ProgressBar progress={0} color={primaryBlue} />
      <View style={styles.screenContainer}>
        <Card style={styles.card}>
          <Card.Title title={'Step One'} subtitle={'Identify Goal and Measurable Achievement'} left={() => <GoalIcon size={24}/>}/>
        </Card>
        <Card style={styles.card}>
          <Card.Title title={'Step Two'} subtitle={'Determine Reward or Punishment'} left={() => <RewardIcon size={24}/>}/>
        </Card>
        <Card style={styles.card}>
          <Card.Title title={'Step Three'} subtitle={'Recruit Solid Accountability Partners'} left={() => <MembersIcon size={24}/>}/>
        </Card>
        <Button style={styles.button} labelStyle={styles.buttonText} color={primaryBlue} uppercase={false} mode='text'
                onPress={() => navigation.navigate('GroupName')}>Begin</Button>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  screenContainer: {
    flex: 1,
    padding: 16,
  },
  card: {
    marginTop: 16,
  },
  button: {
    marginTop: 16,
  },
  buttonText: {
    fontSize: 20,
  },
});