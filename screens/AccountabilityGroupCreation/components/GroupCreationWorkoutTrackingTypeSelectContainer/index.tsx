import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {UiCreateGroupFormActions} from '../../../../actions/ui/UiCreateGroupFormActions';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';
import {GroupCreationWorkoutTrackingTypeSelect} from '../GroupCreationWorkoutTrackingTypeSelect';

export interface GroupCreationWorkoutTrackingTypeSelectContainerProps {
  selectedTrackingType?: string;
  time?: string;
  distance?: string;
  repetitions?: string;
  uiCreateGroupFormActions?: UiCreateGroupFormActions;
}

@connect(
  (state: RootState): Partial<GroupCreationWorkoutTrackingTypeSelectContainerProps> => ({
    selectedTrackingType: state.ui.createGroupForm.selectedTrackingType,
    time: state.ui.createGroupForm.time,
    distance: state.ui.createGroupForm.distance,
    repetitions: state.ui.createGroupForm.repetitions,
  }),
  (dispatch: Dispatch): Partial<GroupCreationWorkoutTrackingTypeSelectContainerProps> => ({
    uiCreateGroupFormActions: bindActionCreators(omit(UiCreateGroupFormActions, 'Type'), dispatch),
  })
)
export class GroupCreationWorkoutTrackingTypeSelectContainer extends React.Component<GroupCreationWorkoutTrackingTypeSelectContainerProps> {
  render() {
    return <GroupCreationWorkoutTrackingTypeSelect selectedTrackingType={this.props.selectedTrackingType!}
                                                   time={this.props.time}
                                                   distance={this.props.distance}
                                                   repetitions={this.props.repetitions}
                                                   onTimeChange={this.onTimeChange.bind(this)}
                                                   onDistanceChange={this.onDistanceChange.bind(this)}
                                                   onRepetitionsChange={this.onRepetitionsChange.bind(this)}
                                                   onSelectionChange={this.onSelectionChange.bind(this)}/>;
  }

  onSelectionChange(selection: string): void {
    this.props.uiCreateGroupFormActions!.setSelectedTrackingType(selection);
  }

  onTimeChange(input: string): void {
    this.props.uiCreateGroupFormActions!.setTimeInput(input);
  }

  onDistanceChange(input: string): void {
    this.props.uiCreateGroupFormActions!.setDistanceInput(input);
  }

  onRepetitionsChange(input: string): void {
    this.props.uiCreateGroupFormActions!.setRepetitionsInput(input);
  }
}