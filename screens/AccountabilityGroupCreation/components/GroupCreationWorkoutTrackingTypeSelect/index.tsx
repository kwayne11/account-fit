import * as React from 'react';
import {StyleSheet, TextInput} from 'react-native';
import {Chip, Subheading} from 'react-native-paper';
import {View} from '../../../../components/Themed';
import {darkGrey, primaryBlue} from '../../../../constants/Colors';
import {ExerciseTrackingTypeDisplayMapping} from '../../../../models/data/ExerciseTrackingTypeDisplayMapping';
import {ExerciseTrackingType} from '../../../../models/entities/ExerciseTrackingType';

export interface GroupCreationWorkoutTrackingTypeSelectProps {
  selectedTrackingType: string;
  time?: string;
  distance?: string;
  repetitions?: string;
  onSelectionChange: (activity: string) => void;
  onTimeChange: (input: string) => void;
  onDistanceChange: (input: string) => void;
  onRepetitionsChange: (input: string) => void;
}

export class GroupCreationWorkoutTrackingTypeSelect extends React.Component<GroupCreationWorkoutTrackingTypeSelectProps> {
  render() {
    return (
      <>
        <Subheading style={styles.subheading}>Select Tracking Type:</Subheading>
        <View style={styles.chips}>
          <Chip icon="timer" selected={this.props.selectedTrackingType === ExerciseTrackingType.TIME}
                onPress={() => this.props.onSelectionChange(ExerciseTrackingType.TIME)}>Time</Chip>
          <Chip icon="ray-start-arrow" selected={this.props.selectedTrackingType === ExerciseTrackingType.DISTANCE}
                onPress={() => this.props.onSelectionChange(ExerciseTrackingType.DISTANCE)}>Distance</Chip>
          <Chip icon="counter" selected={this.props.selectedTrackingType === ExerciseTrackingType.REPETITIONS}
                onPress={() => this.props.onSelectionChange(ExerciseTrackingType.REPETITIONS)}>Repetitions</Chip>
        </View>
        <Subheading style={styles.subheading}>
          {ExerciseTrackingTypeDisplayMapping[this.props.selectedTrackingType]?.displayText}:
        </Subheading>
        <View style={styles.inputs}>
          {this.props.selectedTrackingType === ExerciseTrackingType.TIME &&
          <><TextInput style={styles.input} keyboardType='number-pad' value={this.props.time}
                       onChangeText={this.props.onTimeChange}/>
            <Subheading style={styles.unit}>Minute(s)</Subheading>
          </>}
          {this.props.selectedTrackingType === ExerciseTrackingType.DISTANCE &&
          <>
            <TextInput style={styles.input} keyboardType='decimal-pad' value={this.props.distance}
                       onChangeText={this.props.onDistanceChange}/>
            <Subheading style={styles.unit}>Mile(s)</Subheading>
          </>}
          {this.props.selectedTrackingType === ExerciseTrackingType.REPETITIONS &&
          <><TextInput style={styles.input} keyboardType='number-pad' value={this.props.repetitions}
                       onChangeText={this.props.onRepetitionsChange}/>
            <Subheading style={styles.unit}>Rep(s)</Subheading>
          </>}
        </View>
      </>
    )
  }
}

const styles = StyleSheet.create({
  subheading: {
    color: darkGrey,
    marginTop: 24,
    marginBottom: 16,
  },
  chips: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    flex: 1,
  },
  inputs: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    textAlign: 'center',
    height: 35,
    width: 70,
    fontSize: 16,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: primaryBlue,
  },
  unit: {
    marginLeft: 8,
  }
});