import * as React from 'react';
import {SafeAreaView, ScrollView, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {UserLoadedContainer} from '../../components/loaded/User';
import {Text, View} from '../../components/Themed';
import {RootState} from '../../models/state/RootState';
import SignOutButton from '../SignIn/components/SignOut';
import {CreateAccountabilityGroupFAB} from './components/CreateAccountabilityGroupFAB';
import {UserGroupsSectionContainerWrapper} from './components/UserGroupsSectionContainer';

export interface ProfileScreenProps {
  givenName?: string;
}

@connect(
  (state: RootState) => ({
    givenName: state.data.userId.user?.name,
  }),
  null,
)
export class ProfileScreen extends React.Component<ProfileScreenProps> {
  render() {
    return (
      <SafeAreaView style={styles.safeAreaView}>
        <View style={styles.screenContainer}>
          <UserLoadedContainer>
            <SignOutButton/>
            <View style={styles.container}>
              <Text style={styles.title}>Welcome, {this.props.givenName}</Text>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.userGroupsSection}>
                <UserGroupsSectionContainerWrapper/>
              </View>
            </ScrollView>
            <CreateAccountabilityGroupFAB/>
          </UserLoadedContainer>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  userGroupsSection: {
    flex: 1,
  },
  container: {
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  screenContainer: {
    flex: 1,
    padding: 16,
  },
});
