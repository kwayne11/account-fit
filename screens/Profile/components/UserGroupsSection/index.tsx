import * as React from 'react';
import {StyleSheet} from 'react-native';
import {List, Subheading} from 'react-native-paper';
import {darkGrey} from '../../../../constants/Colors';
import {GroupModel} from '../../../../models/entities/GroupModel';

export interface UserGroupsSectionProps {
  groups: GroupModel[];
  onGroupClick: (value: string) => void;
}

export class UserGroupsSection extends React.Component<UserGroupsSectionProps> {
  render() {
    return <>
      <Subheading style={styles.subheading}>My Accountability Groups</Subheading>
      <List.Section>
        {this.props.groups.map((group: GroupModel) => {
          const numberOfMembers: number = group.members.length;
          const membersList: string = group.members.join(', ');
          const description: string = `${numberOfMembers} ${numberOfMembers > 1 ? 'members' : 'member'}`;
          return <List.Item key={group.uid}
                            title={group.name}
                            onPress={() => this.props.onGroupClick(group.uid)}
                            description={`${description}: ${membersList}`}
                            left={() => <List.Icon icon="account-group"/>}/>
        })}
      </List.Section>
    </>;
  }
}

const styles = StyleSheet.create({
  subheading: {
    color: darkGrey,
    marginTop: 24,
  },
});
