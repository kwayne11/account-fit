import React from 'react';
import {StyleSheet} from 'react-native';
import {FAB} from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

export interface CreateAccountabilityGroupFABProps {
  navigation?: any;
}

export function CreateAccountabilityGroupFAB() {
  const navigation = useNavigation();

  return <FAB style={styles.fab} label='Accountability Group' icon="plus"
               onPress={() => navigation.navigate('AccountabilityGroupCreation')}>
    Create new accountability group
  </FAB>;
}

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});