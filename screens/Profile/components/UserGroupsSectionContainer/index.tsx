import {useNavigation} from '@react-navigation/native';
import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {GroupApiActions} from '../../../../actions/data/GroupApiActions';
import {GroupModel} from '../../../../models/entities/GroupModel';
import {UserModel} from '../../../../models/entities/UserModel';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';
import {UserGroupsSection} from '../UserGroupsSection';

export interface UserGroupsSectionContainerProps {
  navigation?: any;
  user?: UserModel;
  groups?: GroupModel[];
  groupApiActions?: GroupApiActions;
}

@connect(
  (state: RootState): Partial<UserGroupsSectionContainerProps> => ({
    user: state.data.userId.user,
    groups: state.data.userGroups.groups,
  }),
  (dispatch: Dispatch): Partial<UserGroupsSectionContainerProps> => ({
    groupApiActions: bindActionCreators(omit(GroupApiActions, 'Type'), dispatch),
  })
)
export class UserGroupsSectionContainer extends React.Component<UserGroupsSectionContainerProps> {
  render() {
    return <UserGroupsSection groups={this.props.groups!} onGroupClick={this.onGroupClick.bind(this)}/>;
  }

  componentDidMount() {
    this.props.groupApiActions!.loadUserGroupsRequested(this.props.user!);
  }

  onGroupClick(value: string): void {
    this.props.navigation.navigate('GroupDetailsScreen', {groupId: value});
  }
}

export function UserGroupsSectionContainerWrapper(props: UserGroupsSectionContainerProps) {
  const navigation = useNavigation();
  return <UserGroupsSectionContainer {...props} navigation={navigation}/>;
}
