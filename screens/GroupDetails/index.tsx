import {useNavigation} from '@react-navigation/native';
import * as React from 'react';
import {SafeAreaView, ScrollView, StyleSheet} from 'react-native';
import {Appbar} from 'react-native-paper';
import {UserLoadedContainer} from '../../components/loaded/User';
import {View} from '../../components/Themed';
import {GroupInformationSectionContainerWrapper} from './components/GroupInformationSectionContainer';

export function GroupDetailsScreen() {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={styles.safeAreaView}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => {
          return navigation.goBack();
        }}/>
        <Appbar.Content title="Group Details"/>
      </Appbar.Header>
      <View style={styles.screenContainer}>
        <UserLoadedContainer>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.groupInformationSection}>
              <GroupInformationSectionContainerWrapper/>
            </View>
          </ScrollView>
        </UserLoadedContainer>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  groupInformationSection: {
    flex: 1,
  },
  container: {
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  screenContainer: {
    flex: 1,
    padding: 16,
  },
});
