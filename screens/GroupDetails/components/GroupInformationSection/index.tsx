import * as React from 'react';
import {StyleSheet} from 'react-native';
import {Chip, Subheading, Title} from 'react-native-paper';
import {View} from '../../../../components/Themed';
import {darkGrey} from '../../../../constants/Colors';
import {GroupModel} from '../../../../models/entities/GroupModel';
import {UserModel} from '../../../../models/entities/UserModel';

export interface GroupInformationSectionProps {
  user: UserModel;
  group: GroupModel;
}

export class GroupInformationSection extends React.Component<GroupInformationSectionProps> {
  render() {
    return <View>
      <View style={styles.container}>
        <Title>{this.props.group.name}</Title>
      </View>
      <Subheading style={styles.subheading}>Members:</Subheading>
      <View style={styles.chips}>
        {this.props.group.members.map((member: string) => {
          return <Chip key={member} style={styles.chip} icon={'account'}>{member}</Chip>
        })}
      </View>
    </View>;
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
  },
  subheading: {
    color: darkGrey,
    marginTop: 24,
    marginBottom: 16,
  },
  chips: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
    flex: 1,
  },
  chip: {
    marginBottom: 8,
  }
});