import {useRoute} from '@react-navigation/native';
import * as React from 'react';
import {connect} from 'react-redux';
import {GroupModel} from '../../../../models/entities/GroupModel';
import {UserModel} from '../../../../models/entities/UserModel';
import {RootState} from '../../../../models/state/RootState';
import {GroupInformationSection} from '../GroupInformationSection';

export interface GroupInformationSectionContainerProps {
  groupId?: string;
  user?: UserModel;
  group?: GroupModel;
}

@connect(
  (state: RootState, ownProps: Partial<GroupInformationSectionContainerProps>): Partial<GroupInformationSectionContainerProps> => {
    return {
      groupId: ownProps.groupId,
      user: state.data.userId.user,
      group: (state.data.userGroups.groups || []).find((group: GroupModel) => group.uid === ownProps.groupId!),
    }
  },
  null,
)
export class GroupInformationSectionContainer extends React.Component<GroupInformationSectionContainerProps> {
  render() {
    return <GroupInformationSection group={this.props.group!} user={this.props.user!}/>;
  }
}

export function GroupInformationSectionContainerWrapper(props: GroupInformationSectionContainerProps) {
  const route = useRoute();
  // @ts-ignore
  const {groupId} = route.params!;
  return <GroupInformationSectionContainer {...props} groupId={groupId}/>;
}
