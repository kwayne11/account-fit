import * as React from 'react';
import {StyleSheet} from 'react-native';
import {Card, Paragraph} from 'react-native-paper';
import {ActivityStatus} from '../../../../models/data/ActivityStatus';
import {FitnessTaskStatus} from '../FitnessTaskStatus';

export interface FitnessTaskCardProps {
  title: string;
  subtitle: string;
  remainingReps: number;
  activityStatus: ActivityStatus;
}

export class FitnessTaskCard extends React.Component<FitnessTaskCardProps> {
  render() {
    return <Card style={styles.taskCard}>
      <Card.Title title={this.props.title} subtitle={this.props.subtitle}/>
      <Card.Content>
        <Paragraph>{this.props.remainingReps} reps left</Paragraph>
        <FitnessTaskStatus activityStatus={this.props.activityStatus}/>
      </Card.Content>
    </Card>
  }
}

const styles = StyleSheet.create({
  taskCard: {
    marginTop: 20,
    width: '47%',
  },
});
