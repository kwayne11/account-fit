import * as React from 'react';
import {StyleSheet} from 'react-native';
import {Subheading} from 'react-native-paper';
import {View} from '../../../../components/Themed';
import {darkGrey} from '../../../../constants/Colors';
import {ActivityStatus} from '../../../../models/data/ActivityStatus';
import {FitnessTaskCard} from '../FitnessTaskCard';

export class FitnessTaskSection extends React.Component {
  render() {
    return <>
      <Subheading style={styles.subheading}>Your Fitness Tasks</Subheading>
      <View style={styles.tasks}>
        <FitnessTaskCard title='Push-Ups' subtitle='Work Devs' remainingReps={90}
                         activityStatus={ActivityStatus.NOT_STARTED}/>
        <FitnessTaskCard title='Squats' subtitle='Hooligans' remainingReps={20}
                         activityStatus={ActivityStatus.IN_PROGRESS}/>
      </View>
    </>
  }
}

const styles = StyleSheet.create({
  subheading: {
    color: darkGrey,
    marginTop: 24,
  },
  tasks: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
});
