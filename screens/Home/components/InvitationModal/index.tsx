import * as React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import Modal from 'react-native-modal';
import {Headline, IconButton} from 'react-native-paper';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {UiModalActions} from '../../../../actions/ui/UiModalActions';
import {View} from '../../../../components/Themed';
import {primaryBlue} from '../../../../constants/Colors';
import {UiModalSetVisibilityActionModel} from '../../../../models/actions/UiModalSetVisibilityActionModel';
import {GroupModel} from '../../../../models/entities/GroupModel';
import {RootState} from '../../../../models/state/RootState';
import {UiModalSelector} from '../../../../reducers/ui/modal';
import {omit} from '../../../../utils';
import {InvitationListContainer} from '../InvitationListContainer';

export interface InvitationModalProps {
  componentKey?: string;
  invitations?: GroupModel[],
  isVisible?: boolean;
  uiModalActions?: UiModalActions;
}

@connect(
  (state: RootState, props: Partial<InvitationModalProps>): Partial<InvitationModalProps> => ({
    isVisible: UiModalSelector(state, props.componentKey!),
    invitations: state.data.invitationGroups.groups || [],
  }),
  (dispatch: Dispatch): Partial<InvitationModalProps> => ({
    uiModalActions: bindActionCreators(omit(UiModalActions, 'Type'), dispatch),
  })
)
export class InvitationModal extends React.Component<InvitationModalProps> {
  render() {
    return (
      <Modal style={{margin: 0}} propagateSwipe isVisible={this.props.isVisible!} onDismiss={this.onDismiss.bind(this)}
             onBackdropPress={this.onDismiss.bind(this)}>
        <SafeAreaView style={styles.actions}>
          <View style={styles.modalView}>
            <IconButton icon="window-close" color={primaryBlue} size={28} onPress={this.onDismiss.bind(this)}/>
            <View style={styles.invites}>
              {this.props.invitations!.length > 0 ? <InvitationListContainer/> :
                <View style={styles.notifications}>
                  <Headline>No Notifications</Headline>
                </View>}
            </View>
          </View>
        </SafeAreaView>
      </Modal>
    );
  }

  onDismiss(): void {
    this.props.uiModalActions!.setVisible({
      key: this.props.componentKey!,
      isVisible: false,
    } as UiModalSetVisibilityActionModel);
  }
}

const styles = StyleSheet.create({
  actions: {
    flex: 1,
  },
  modalView: {
    flex: 1,
  },
  invites: {
    flex: 1,
    marginTop: 16
  },
  notifications: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});