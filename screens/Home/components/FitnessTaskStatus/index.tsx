import {Entypo} from '@expo/vector-icons';
import * as React from 'react';
import {StyleSheet} from 'react-native';
import {Paragraph} from 'react-native-paper';
import {View} from '../../../../components/Themed';
import {ActivityStatus} from '../../../../models/data/ActivityStatus';
import {ActivityStatusDisplayModel, ActivityStatusIconMapping} from '../../../../models/ui/ActivityStatusIconMapping';

export interface FitnessTaskStatusProps {
  activityStatus: ActivityStatus;
}

export class FitnessTaskStatus extends React.Component<FitnessTaskStatusProps> {
  render() {
    const activityStatusDisplayModel: ActivityStatusDisplayModel = ActivityStatusIconMapping[this.props.activityStatus];
    return <View style={styles.progress}>
      <Entypo name={activityStatusDisplayModel.icon} size={16} color={activityStatusDisplayModel.color}/>
      <Paragraph style={styles.paragraph}>{activityStatusDisplayModel.text}</Paragraph>
    </View>
  }
}

const styles = StyleSheet.create({
  paragraph: {
    marginLeft: 8,
  },
  progress: {
    backgroundColor: 'transparent',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
});