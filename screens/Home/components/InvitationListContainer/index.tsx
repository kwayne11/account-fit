import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {GroupApiActions} from '../../../../actions/data/GroupApiActions';
import {GroupModel} from '../../../../models/entities/GroupModel';
import {UserModel} from '../../../../models/entities/UserModel';
import {RootState} from '../../../../models/state/RootState';
import {omit} from '../../../../utils';
import {InvitationList} from '../InvitationList';

export interface InvitationListContainerProps {
  user?: UserModel;
  groups?: GroupModel[];
  groupApiActions?: GroupApiActions;
}

@connect(
  (state: RootState): Partial<InvitationListContainerProps> => ({
    user: state.data.userId.user,
    groups: state.data.invitationGroups.groups,
  }),
  (dispatch: Dispatch): Partial<InvitationListContainerProps> => ({
    groupApiActions: bindActionCreators(omit(GroupApiActions, 'Type'), dispatch),
  })
)
export class InvitationListContainer extends React.Component<InvitationListContainerProps> {
  constructor(props: InvitationListContainerProps) {
    super(props);
    this.onYesClick = this.onYesClick.bind(this);
    this.onNoClick = this.onNoClick.bind(this);
  }

  render() {
    return <InvitationList groups={this.props.groups || []}
                           onYesClick={this.onYesClick}
                           onNoClick={this.onNoClick}
    />;
  }

  onYesClick(group: GroupModel): void {
    this.props.groupApiActions!.acceptGroupInvitationRequested(group);
  }

  onNoClick(group: GroupModel): void {
    this.props.groupApiActions!.rejectGroupInvitationRequested(group);
  }
}
