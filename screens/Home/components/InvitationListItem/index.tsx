import * as React from 'react';
import {IconButton, List} from 'react-native-paper';
import {GroupModel} from '../../../../models/entities/GroupModel';

export interface InvitationListItemProps {
  group: GroupModel;
  onYesClick: () => void;
  onNoClick: () => void;
}

export class InvitationListItem extends React.Component<InvitationListItemProps> {
  render() {
    const numberOfMembers: number = this.props.group.members.length;
    const membersList: string = this.props.group.members.join(', ');
    const description: string = `${numberOfMembers} ${numberOfMembers > 1 ? 'members' : 'member'}`;
    return <List.Item key={this.props.group.uid} title={this.props.group.name}
                      description={`${description}: ${membersList}`}
                      left={() => <List.Icon icon="account-multiple-plus"/>}
                      right={() => <>
                        <IconButton onPress={this.props.onYesClick} color='#4CAF50' icon="alpha-y-box-outline"/>
                        <IconButton onPress={this.props.onNoClick} color='#F43366' icon="alpha-n-box-outline"/>
                      </>}
    />;
  }
}
