import * as React from 'react';
import {StyleSheet} from 'react-native';
import {Subheading} from 'react-native-paper';
import {PushupIcon} from '../../../../components/common/PushupIcon';
import {RunningIcon} from '../../../../components/common/RunningIcon';
import {SquatIcon} from '../../../../components/common/SquatIcon';
import {darkGrey} from '../../../../constants/Colors';
import {RecentActivityCard} from '../RecentActivityCard';

const RunningContent = (props: any) => <RunningIcon size={30} {...props}/>
const SquatContent = (props: any) => <SquatIcon size={30} {...props}/>
const PushupContent = (props: any) => <PushupIcon size={30} {...props}/>

export class RecentActivitySection extends React.Component {
  render() {
    return <>
      <Subheading style={styles.subheading}>Recent Activity</Subheading>
      <RecentActivityCard title='Emily: Run Completed!' subtitle='2 Miles' date='July 28th, 2020'
                          titleLeftContent={RunningContent}/>
      <RecentActivityCard title='Kevin: Push-Ups Completed!' subtitle='100 Push-Ups' date='July 27th, 2020'
                          titleLeftContent={PushupContent}/>
      <RecentActivityCard title='Steven: Squats Completed!' subtitle='50 Squats' date='July 26th, 2020'
                          titleLeftContent={SquatContent}/>
    </>;
  }
}

const styles = StyleSheet.create({
  subheading: {
    color: darkGrey,
    marginTop: 24,
  },
});
