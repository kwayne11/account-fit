import * as React from 'react';
import {GroupModel} from '../../../../models/entities/GroupModel';
import {InvitationListItem} from '../InvitationListItem';

export interface InvitationListProps {
  groups: GroupModel[];
  onYesClick: (group: GroupModel) => void;
  onNoClick: (group: GroupModel) => void;
}

export class InvitationList extends React.Component<InvitationListProps> {
  render() {
    return this.props.groups.map((group: GroupModel) => {
      return <InvitationListItem key={group.uid} group={group} onYesClick={() => this.props.onYesClick(group)} onNoClick={() => this.props.onNoClick(group)}/>;
    })
  }
}
