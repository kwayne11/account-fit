import React from 'react';
import {StyleSheet} from 'react-native';
import {Card, Paragraph} from 'react-native-paper';


export interface RecentActivityCardProps {
  title: string;
  subtitle: string;
  date: string;
  titleLeftContent?: ((props: {
    size: number;
  }) => React.ReactNode) | undefined;
}

export class RecentActivityCard extends React.Component<RecentActivityCardProps> {
  render() {
    return <Card style={styles.card}>
      <Card.Title title={this.props.title} subtitle={this.props.subtitle} left={this.props.titleLeftContent}/>
      <Card.Content>
        <Paragraph>{this.props.date}</Paragraph>
      </Card.Content>
    </Card>
  }
}

const styles = StyleSheet.create({
  card: {
    marginTop: 16,
  },
});
