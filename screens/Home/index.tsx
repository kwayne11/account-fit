import * as React from 'react';
import {SafeAreaView, ScrollView, StyleSheet} from 'react-native';
import {UserLoadedContainer} from '../../components/loaded/User';
import {LogoHeaderContainer} from '../../components/LogoHeaderContainer';
import {View} from '../../components/Themed';
import {FitnessTaskSection} from './components/FitnessTasksSection';
import {InvitationModal} from './components/InvitationModal';
import {RecentActivitySection} from './components/RecentActivitySection';

export class HomeScreen extends React.Component {
  render() {
    return <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.screenContainer}>
        <UserLoadedContainer>
          <InvitationModal componentKey='invitation_modal'/>
          <LogoHeaderContainer componentKey='invitation_modal'/>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.bodyContainer}>
              <FitnessTaskSection/>
              <RecentActivitySection/>
            </View>
          </ScrollView>
        </UserLoadedContainer>
      </View>
    </SafeAreaView>
  }
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  screenContainer: {
    flex: 1,
    padding: 16,
  },
  bodyContainer: {
    flex: 1,
  },
});
