import * as AppleAuthentication from 'expo-apple-authentication';
import React from 'react';
import {Image, SafeAreaView, StyleSheet} from 'react-native';
import {Button} from 'react-native-paper';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {AppleAuthenticationApiActions} from '../../actions/data/AppleAuthenticationApiActions';
import {GoogleAuthenticationApiActions} from '../../actions/data/GoogleAuthenticationApiActions';
import {View} from '../../components/Themed';
import {primaryBlue} from '../../constants/Colors';
import {RootState} from '../../models/state/RootState';
import {omit} from '../../utils';

export interface SignInScreenProps {
  isAuthenticated?: boolean;
  googleAuthenticationApiActions?: GoogleAuthenticationApiActions;
  appleAuthenticationApiActions?: AppleAuthenticationApiActions;
}

@connect(
  (state: RootState) => ({
    isAuthenticated: state.auth.isAuthenticated,
  }),
  (dispatch: Dispatch): Partial<SignInScreenProps> => ({
    googleAuthenticationApiActions: bindActionCreators(omit(GoogleAuthenticationApiActions, 'Type'), dispatch),
    appleAuthenticationApiActions: bindActionCreators(omit(AppleAuthenticationApiActions, 'Type'), dispatch),
  })
)
export default class SignInScreen extends React.Component<SignInScreenProps> {
  constructor(props: any) {
    super(props);
    this.googleSignIn = this.googleSignIn.bind(this);
    this.appleSignIn = this.appleSignIn.bind(this);
  }

  render() {
    return (
      <SafeAreaView style={styles.safeAreaView}>
        <View style={styles.signin}>
          <Image style={{ width: 300, height: 300 }} source={require('../../assets/images/account_fit_logo.png')}/>
          <View style={styles.loginButtons}>
            <Button style={styles.google} uppercase={false} icon="google" mode="contained" onPress={this.googleSignIn}>Sign
              in with Google</Button>
            <AppleAuthentication.AppleAuthenticationButton
              buttonType={AppleAuthentication.AppleAuthenticationButtonType.SIGN_IN}
              buttonStyle={AppleAuthentication.AppleAuthenticationButtonStyle.BLACK}
              cornerRadius={5}
              style={{width: '100%', height: 44}} onPress={this.appleSignIn}>
            </AppleAuthentication.AppleAuthenticationButton>
          </View>
        </View>
      </SafeAreaView>
    )
  }

  googleSignIn(): void {
    this.props.googleAuthenticationApiActions!.signInRequested();
  }

  appleSignIn(): void {
    this.props.appleAuthenticationApiActions!.signInRequested();
  }
}

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
    backgroundColor: primaryBlue
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  signin: {
    flex: 1,
    backgroundColor: primaryBlue,
    alignItems: "center",
    justifyContent: "center"
  },
  loginButtons: {
    width: '80%',
    backgroundColor: primaryBlue,
  },
  google: {
    height: 44,
    marginBottom: 10,
    alignItems: "center",
    justifyContent: "center"
  }
})