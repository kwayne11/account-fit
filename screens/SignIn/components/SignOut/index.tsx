import React from 'react';
import {Button} from 'react-native-paper';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import {FirebaseAuthenticationApiActions} from '../../../../actions/data/FirebaseAuthenticationApiActions';
import {primaryBlue} from '../../../../constants/Colors';
import {omit} from '../../../../utils';

export interface SignOutButtonProps {
  firebaseAuthenticationApiActions?: FirebaseAuthenticationApiActions;
}

@connect(
  null,
  (dispatch: Dispatch): Partial<SignOutButtonProps> => ({
    firebaseAuthenticationApiActions: bindActionCreators(omit(FirebaseAuthenticationApiActions, 'Type'), dispatch),
  })
)
export default class SignOutButton extends React.Component<SignOutButtonProps> {
  constructor(props: any) {
    super(props);
    this.signOut = this.signOut.bind(this);
  }

  render() {
    return (
      <Button color={primaryBlue} uppercase={false} mode='text' onPress={this.signOut}>Sign Out</Button>
    )
  }

  signOut(): void {
    this.props.firebaseAuthenticationApiActions!.signOutRequested();
  }
}
