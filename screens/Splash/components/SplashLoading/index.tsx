import React from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import {ActivityIndicator} from 'react-native-paper';
import {primaryBlue} from '../../../../constants/Colors';

export function SplashLoading() {
  return (
    <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.container}>
        <ActivityIndicator animating={true} color={primaryBlue} size='large'/>
      </View>
    </SafeAreaView>
  );

}

const styles: any = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});