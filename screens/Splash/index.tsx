import * as firebase from 'firebase';
import {User} from 'firebase';
import React, {useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {useDispatch} from 'react-redux';
import {FirebaseAuthenticationApiActions} from '../../actions/data/FirebaseAuthenticationApiActions';
import {SplashLoading} from './components/SplashLoading';

// @ts-ignore
export function SplashScreen({navigation}) {

  const dispatch = useDispatch();

  useEffect(() => {
    navigateToAuthOrHomeScreen()
  }, [navigation, dispatch])

  function navigateToAuthOrHomeScreen() {
    setTimeout(() => {
      firebase.auth().onAuthStateChanged(
        async (authUser: User | null) => {
          if (authUser !== null) {
            console.log('with user in auth changed');
            dispatch(FirebaseAuthenticationApiActions.loadUserRequested(authUser));
            navigation.reset(
              {
                index: 0,
                routes: [{name: 'Root'}]
              }
            )
          } else {
            console.log('no user in auth changed');
            navigation.reset(
              {
                index: 0,
                routes: [{name: 'SignIn'}]
              }
            )
          }
        }
      );
    })
  }

  return <SplashLoading/>;

}

const styles: any = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});