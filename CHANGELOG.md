# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.27](https://gitlab.com/kwayne11/account-fit/compare/v1.0.25...v1.0.27) (2020-09-27)

### [1.0.26](https://gitlab.com/kwayne11/account-fit/compare/v1.0.25...v1.0.26) (2020-09-27)

### [1.0.25](https://gitlab.com/kwayne11/account-fit/compare/v1.0.24...v1.0.25) (2020-09-11)

### [1.0.24](https://gitlab.com/kwayne11/account-fit/compare/v1.0.23...v1.0.24) (2020-09-03)

### [1.0.23](https://gitlab.com/kwayne11/account-fit/compare/v1.0.22...v1.0.23) (2020-08-31)

### [1.0.22](https://gitlab.com/kwayne11/account-fit/compare/v1.0.21...v1.0.22) (2020-08-30)

### 1.0.21 (2020-08-30)
