import {handleActions} from 'redux-actions';
import {GroupApiActions} from '../../../actions/data/GroupApiActions';
import {UserGroupsDataModel} from '../../../models/state/data/UserGroupsDataModel';
import {IDataModelHandlers} from '../handlers/IDataModelHandlers';

const initialState: UserGroupsDataModel = {
  isLoading: false,
  isLoaded: false,
  groups: [],
};

const handlers: any = {
  [GroupApiActions.Type.FIREBASE_LOAD_USER_GROUPS_REQUESTED]: IDataModelHandlers.setIsLoading,
  [GroupApiActions.Type.FIREBASE_LOAD_USER_GROUPS_SUCCEEDED]: IDataModelHandlers.setPayload('groups'),

};

export const userGroupsReducer: any = handleActions(handlers, initialState);