import {Action, handleActions} from 'redux-actions';
import {EnvironmentActions} from '../../../actions/data/EnvironmentActions';
import {RootState} from '../../../models/state/RootState';

const initialState: RootState.EnvironmentState = {
  isLoaded: false,
}

const handlers: any = {
  [EnvironmentActions.Type.ENVIRONMENT_LOAD_SUCCEEDED]: (state: RootState.EnvironmentState, action: Action<RootState.EnvironmentState>) => {
    return action.payload;
  },
};

export const environmentReducer: any = handleActions(handlers, initialState);