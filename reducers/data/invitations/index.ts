import {handleActions} from 'redux-actions';
import {GroupApiActions} from '../../../actions/data/GroupApiActions';
import {InvitationGroupsDataModel} from '../../../models/state/data/InvitationGroupsDataModel';
import {IDataModelHandlers} from '../handlers/IDataModelHandlers';

const initialState: InvitationGroupsDataModel = {
  isLoading: false,
  isLoaded: false,
  groups: [],
};

const handlers: any = {
  [GroupApiActions.Type.FIREBASE_LOAD_USER_INVITATIONS_REQUESTED]: IDataModelHandlers.setIsLoading,
  [GroupApiActions.Type.FIREBASE_LOAD_USER_INVITATIONS_SUCCEEDED]: IDataModelHandlers.setPayload('groups'),

};

export const invitationGroupsReducer: any = handleActions(handlers, initialState);