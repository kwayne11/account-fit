import {combineReducers} from 'redux';
import {RootState} from '../../models/state/RootState';
import {userGroupsReducer} from './groups';
import {invitationGroupsReducer} from './invitations';
import {userIdReducer} from './userId';

export const dataReducer = combineReducers<RootState.DataState>({
  invitationGroups: invitationGroupsReducer,
  userId: userIdReducer,
  userGroups: userGroupsReducer,
})