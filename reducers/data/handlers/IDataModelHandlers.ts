import {Action} from 'redux-actions';
import {ILoadable} from '../../../models/state/data/ILoadable';

export const IDataModelHandlers = {
    setPayload: (property: string) => {
        return (state: ILoadable, action: Action<any>): any => {
            return {...state, [property]: action.payload!, isLoading: false, isLoaded: true}
        };
    },

    setIsLoading: (state: ILoadable): any => {
        return {...state, isLoading: true, isLoaded: false};
    },

    setLoaded: (state: ILoadable): any => {
        return {...state, isLoaded: true, isLoading: false}
    }
}