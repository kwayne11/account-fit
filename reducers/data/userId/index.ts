import {handleActions} from 'redux-actions';
import {FirebaseAuthenticationApiActions} from '../../../actions/data/FirebaseAuthenticationApiActions';
import {UserIdDataModel} from '../../../models/state/data/UserIdDataModel';
import {IDataModelHandlers} from '../handlers/IDataModelHandlers';

const initialState: UserIdDataModel = {
  isLoading: false,
  isLoaded: false,
  user: undefined,
};

const handlers: any = {
  [FirebaseAuthenticationApiActions.Type.FIREBASE_SIGN_OUT_SUCCEEDED]: IDataModelHandlers.setPayload('user'),
  [FirebaseAuthenticationApiActions.Type.FIREBASE_LOAD_USER_SUCCEEDED]: IDataModelHandlers.setPayload('user'),

  [FirebaseAuthenticationApiActions.Type.FIREBASE_SIGN_IN_WITH_GOOGLE_REQUESTED]: IDataModelHandlers.setIsLoading,
  [FirebaseAuthenticationApiActions.Type.FIREBASE_SIGN_IN_WITH_APPLE_REQUESTED]: IDataModelHandlers.setIsLoading,
  [FirebaseAuthenticationApiActions.Type.FIREBASE_LOAD_USER_REQUESTED]: IDataModelHandlers.setIsLoading,
};

export const userIdReducer: any = handleActions(handlers, initialState);