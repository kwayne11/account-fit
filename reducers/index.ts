import {combineReducers} from 'redux';
import {RootState} from '../models/state/RootState';
import {authReducer} from './auth';
import {dataReducer} from './data';
import {environmentReducer} from './data/environment';
import {uiReducer} from './ui';

export const createRootReducer = () => combineReducers<RootState>({
  ui: uiReducer,
  environment: environmentReducer,
  data: dataReducer,
  auth: authReducer,
})