import {combineReducers} from 'redux';
import {RootState} from '../../models/state/RootState';
import {createGroupFormReducer} from './createGroupForm';
import {modalReducer} from './modal';

export const uiReducer = combineReducers<RootState.UiState>({
  createGroupForm: createGroupFormReducer,
  modal: modalReducer,
})