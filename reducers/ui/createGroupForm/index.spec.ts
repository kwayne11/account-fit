import {UiCreateGroupFormActions} from '../../../actions/ui/UiCreateGroupFormActions';
import {UiCreateGroupFormModel} from '../../../models/state/ui/UiCreateGroupFormModel';
import {createGroupFormReducer} from './index';

describe('createGroupFormReducer', () => {
  describe('CREATE_GROUP_FORM_EDIT_NAME', () => {
    it('sets the name of the group', () => {
      const state: UiCreateGroupFormModel = createGroupFormReducer(undefined, UiCreateGroupFormActions.editName('Group Name'));
      expect(state.name).toEqual('Group Name');
    });
  });

  describe('CREATE_GROUP_FORM_EDIT_NAME', () => {
    it('sets the name of the group', () => {
      const state: UiCreateGroupFormModel = createGroupFormReducer(undefined, UiCreateGroupFormActions.editReward('Dinner party!'));
      expect(state.reward).toEqual('Dinner party!');
    });
  });
});