import {Action, handleActions} from 'redux-actions';
import {UiCreateGroupFormActions} from '../../../actions/ui/UiCreateGroupFormActions';
import {ExerciseActivityType} from '../../../models/entities/ExerciseActivityType';
import {ExerciseOptionModel} from '../../../models/entities/ExerciseOptionModel';
import {ExerciseTrackingType} from '../../../models/entities/ExerciseTrackingType';
import {UiCreateGroupFormModel} from '../../../models/state/ui/UiCreateGroupFormModel';

const initialState: UiCreateGroupFormModel = {
  name: '',
  reward: '',
  memberSearch: '',
  invited: [],
  options: [],
  selectedActivity: ExerciseActivityType.CORE_TRAINING,
  selectedTrackingType: ExerciseTrackingType.TIME,
  distance: '2',
  repetitions: '10',
  time: '30',
  daysRequired: '15',
};

const resetForm = (): UiCreateGroupFormModel => {
  return {...initialState};
}

const setName = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  return {...state, name: action.payload!}
}

const setSelectedActivity = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  return {...state, selectedActivity: action.payload!}
}

const setSelectedTrackingType = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  return {...state, selectedTrackingType: action.payload!}
}

const editMemberSearch = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  return {...state, memberSearch: action.payload!}
}

const addMember = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  const existingMember: string | undefined = state.invited.find((member: string) => member === action.payload!);
  if (existingMember) {
    return {...state};
  } else {
    return {
      ...state,
      memberSearch: '',
      invited: [...state.invited, action.payload!]
    }
  }
}

const addOption = (state: UiCreateGroupFormModel, action: Action<ExerciseOptionModel>): UiCreateGroupFormModel => {
  return {
    ...state,
    selectedTrackingType: initialState.selectedTrackingType,
    selectedActivity: initialState.selectedActivity,
    repetitions: initialState.repetitions,
    distance: initialState.distance,
    time: initialState.time,
    options: [...state.options, action.payload!]
  }
}

const removeOption = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  return {
    ...state,
    options: state.options.filter((option: ExerciseOptionModel) => option.uid !== action.payload!),
  }
}

const removeMember = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  return {
    ...state,
    memberSearch: '',
    invited: state.invited.filter((member: string) => member !== action.payload!),
  }
}

const setReward = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  return {...state, reward: action.payload!}
}

const setTime = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  return {...state, time: action.payload!}
}

const setDistance = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  return {...state, distance: action.payload!}
}

const setRepetitions = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  return {...state, repetitions: action.payload!}
}

const setDays = (state: UiCreateGroupFormModel, action: Action<string>): UiCreateGroupFormModel => {
  return {...state, daysRequired: action.payload!}
}

export const createGroupFormReducer: any = handleActions({
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_ADD_OPTION]: addOption as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_REMOVE_OPTION]: removeOption as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_EDIT_NAME]: setName as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_EDIT_REWARD]: setReward as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_ADD_MEMBER]: addMember as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_REMOVE_MEMBER]: removeMember as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_EDIT_MEMBER_SEARCH]: editMemberSearch as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_SET_SELECTED_ACTIVITY]: setSelectedActivity as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_SET_SELECTED_TRACKING_TYPE]: setSelectedTrackingType as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_SET_TIME]: setTime as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_SET_DISTANCE]: setDistance as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_SET_REPETITIONS]: setRepetitions as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_SET_DAYS_REQUIRED]: setDays as any,
  [UiCreateGroupFormActions.Type.CREATE_GROUP_FORM_RESET_FORM]: resetForm as any,
}, initialState);