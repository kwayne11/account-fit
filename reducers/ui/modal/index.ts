import {Action, handleActions} from 'redux-actions';
import {UiModalActions} from '../../../actions/ui/UiModalActions';
import {UiModalSetVisibilityActionModel} from '../../../models/actions/UiModalSetVisibilityActionModel';
import {RootState} from '../../../models/state/RootState';
import {UiModalState} from '../../../models/state/ui/UiModel';

const initialState: UiModalState = {};

const setVisible = (state: UiModalState, action: Action<UiModalSetVisibilityActionModel>): UiModalState => {
  return {
    ...state,
    [action.payload!.key]: {
      isVisible: action.payload!.isVisible,
    }
  }
}

export const modalReducer: any = handleActions({
  [UiModalActions.Type.MODAL_SET_VISIBLE]: setVisible,
}, initialState);

export function UiModalSelector(state: RootState, componentKey: string): boolean {
  if (state.ui.modal[componentKey]) {
    return state.ui.modal[componentKey].isVisible;
  } else {
    return false;
  }
}