import {Action, handleActions} from 'redux-actions';
import {AppleAuthenticationApiActions} from '../../actions/data/AppleAuthenticationApiActions';
import {GoogleAuthenticationApiActions} from '../../actions/data/GoogleAuthenticationApiActions';
import {AuthModel, LoginResult} from '../../models/state/auth/AuthModel';

const initialState: AuthModel = {
  isAuthenticated: false,
  isLoaded: false,
  loginResult: undefined,
};

function setLogInResult(state: AuthModel, action: Action<LoginResult>): AuthModel {
  return {
    ...state,
    isAuthenticated: true,
    isLoaded: true,
    loginResult: action.payload!,
  };
}

const setAuthenticated = (isAuthenticated: boolean) => () => ({isAuthenticated, isLoaded: true});

const handlers: any = {
  [AppleAuthenticationApiActions.Type.APPLE_SIGN_IN_SUCCEEDED]: setLogInResult,
  [AppleAuthenticationApiActions.Type.APPLE_SIGN_IN_FAILED]: setAuthenticated(false),
  [GoogleAuthenticationApiActions.Type.GOOGLE_SIGN_IN_SUCCEEDED]: setLogInResult,
  [GoogleAuthenticationApiActions.Type.GOOGLE_SIGN_IN_FAILED]: setAuthenticated(false),
};

export const authReducer: any = handleActions(handlers, initialState);
