const tintColorLight = '#293d58';
const tintColorDark = '#f2f2f2';
export const primaryBlue: string = '#293d58';
export const secondaryGreen: string = '#6CEAAD';
export const secondaryPurple: string = '#B37BF3';
export const secondaryBlue: string = '#55A2F4';
export const backgroundWhite: string = '#f2f2f2';
export const darkGrey: string = '#696969';

export default {
  light: {
    text: primaryBlue,
    background: backgroundWhite,
    tint: tintColorLight,
    tabIconDefault: primaryBlue,
    tabIconSelected: tintColorLight,
  },
  dark: {
    text: backgroundWhite,
    background: primaryBlue,
    tint: tintColorDark,
    tabIconDefault: backgroundWhite,
    tabIconSelected: tintColorDark,
  },
};
